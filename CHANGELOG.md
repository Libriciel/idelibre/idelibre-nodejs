# Changelog
All notable changes to this project will be documented in this file.


## [4.2.1] 2023-06-02

### Correction   
- fix impossible de télécharger les autres documents


## [4-2-rc1] 2023-02-03
### Evolution
- nodejs 14 => 18

### Nouveauté
- Ajout de autre document
- authentification via token
- gestion des tokens invalidés
- changement de mot de passe via l'api pour avoir l'entropie

### Correction
- ordre des annexes
- ordre des seances archivés

## [4.1] - 2021-??-??
### Nouveauté
- mdp oublié


## [4.0.1] - 2021-08-24

### Corrections
- Correction du calcul du hash du legacy password coté client (sha1)

