FROM ubuntu:22.04 as idelibrefpm

ARG TIMEZONE=UTC
RUN ln -snf /usr/share/zoneinfo/$TIMEZONE /etc/localtime && echo $TIMEZONE > /etc/timezone


RUN apt dist-upgrade -yqq \
    && apt-get update -yqq \
    && apt-get install \
        wget \
        sudo \
        vim \
        curl \
        git \
        zip \
        unzip \
        locales \
        netcat \
        openssl \
        -yqq


RUN curl -sL https://deb.nodesource.com/setup_18.x | sudo bash -
RUN apt-get install nodejs -yqq


RUN apt-get install postgresql-client -yy

COPY . /app
WORKDIR /app

RUN mkdir /data


RUN npm install

EXPOSE 3000

