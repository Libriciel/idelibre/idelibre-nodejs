import { IProcessor } from 'typeorm-fixtures-cli';
import { v4 as uuidV4 } from 'uuid';


export default class UuidProcessor implements IProcessor<any> {
    preProcess(name: string, object: any): any {
        return { ...object, id: uuidV4() };
    }
}
