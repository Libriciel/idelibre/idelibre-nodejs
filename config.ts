import * as dotenv from "dotenv";
dotenv.config();


export default {
    env: process.env.APP_ENV,
    salt: process.env.SALT,
    tokenDirectory: '/data/token/',
    lshorodatageUrl: process.env.LSHORODATAGE_URL,
    nginxIdelibre: 'https://nginx-idelibre',
    logLevel: process.env.LOG_LEVEL,
    logPath: '/tmp/idelibre.log',
    listenPort: process.env.LISTEN_PORT,
    passphrase: process.env.PASSPHRASE,
    jwtSecret: process.env.JWT_SECRET,
    tokenTimeout: process.env.JWT_TIMEOUT,
    zipDirectory: '/data/zip/',
    fullPdfDirectory: '/data/pdf/',
    legacy : {
        groupActor : 'b2f7a7e4-4ab1-4d93-ac13-d7ca540b4c16',
        groupGuest : 'aa130693-e6d0-4893-ba31-98892b98581f',
        groupEmployee : 'db68a3c7-0119-40f1-b444-e96f568b3d67',
        present: 'present',
        absent: 'absent',
        remote: 'remote',
        version: '3.1.0'
    }
}
