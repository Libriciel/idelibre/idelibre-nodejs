import { IProcessor } from 'typeorm-fixtures-cli';
import { v4 as uuidV4 } from 'uuid';
import * as argon2 from 'argon2';

export default class UserProcessor implements IProcessor<any> {
    async preProcess(name: string, object: any): Promise<any> {
        const hashedPassword = await argon2.hash(object.password);

        return { ...object, id: uuidV4(), password: hashedPassword };
    }
}
