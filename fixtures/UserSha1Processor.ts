import { IProcessor } from 'typeorm-fixtures-cli';
import { v4 as uuidV4 } from 'uuid';
import config from "../config";
import * as sha1 from "sha1";


export default class UserProcessor implements IProcessor<any> {
    async preProcess(name: string, object: any): Promise<any> {
        let passwordSalt = config.salt + object.password;
        let passwordSha1 = sha1(passwordSalt);
        return { ...object, id: uuidV4(), password: passwordSha1 };
    }
}
