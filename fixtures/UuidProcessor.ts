import { IProcessor } from 'typeorm-fixtures-cli';
import { v4 as uuidV4 } from 'uuid';

export default class UuidProcessor implements IProcessor<any> {
    async preProcess(name: string, object: any): Promise<any> {
        if(object.id) {
            return object
        }

        return { ...object, id: uuidV4() };
    }
}
