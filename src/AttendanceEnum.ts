export enum AttendanceEnum {
    Present = 'present',
    Absent = 'absent',
    Remote = 'remote',
    AbsentMandator = 'poa',
    AbsentDeputy = 'deputy',
    Undefined = ''
}


