import "reflect-metadata";
import * as express from "express";
import {Request, Response} from "express";
import config from "../config";
import verifyToken from "./middleware/verifyToken";
import {notificationController} from "./controller/0.2.0/NotificationController";
import {fileController} from "./controller/0.2.0/FileController";
import {zipController} from "./controller/0.2.0/ZipController";
import {sittingPdfController} from "./controller/0.2.0/SittingPdfController";
import {securityController} from "./controller/0.2.0/SecurityController";
import {forgetPasswordController} from "./controller/0.2.0/ForgetPasswordController";
import {attendanceController} from "./controller/0.2.0/AttendanceController";

const app = express();
app.use(express.json());

app.get("/version", (req: Request, res: Response) => {
    res.send({version: '4.0.0'});
});

app.get('/checkConnection', function (req, res) {
    res.send(config.legacy.version);
});


app.use('/notifications', notificationController);
app.use('/0.2.0/projets', verifyToken, fileController);
app.use('/0.2.0/otherdocs', verifyToken, fileController);
app.use('/0.2.0/annexes', verifyToken, fileController);
app.use('/0.2.0/zips', verifyToken, zipController);
app.use('/0.2.0/pdf', verifyToken, sittingPdfController);
app.use('/0.2.0/users/changePassword', verifyToken, securityController);
app.use('/forgetPassword', forgetPasswordController);
app.use('/0.2.0/attendance', verifyToken, attendanceController);

export default app
