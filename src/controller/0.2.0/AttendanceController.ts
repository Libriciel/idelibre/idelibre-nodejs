import * as express from "express";
import {getRepository} from "typeorm";
import {Convocation} from "../../entity/Convocation";
import {AttendanceDto} from "../../dto/0.2.0/AttendanceDto";
import {User} from "../../entity/User";
import {attendanceManager} from "../../service/0.2.0/AttendanceManager";
import {Sitting} from "../../entity/Sitting";

export const attendanceController = express.Router();


attendanceController.get('/sittings/:sittingId', async (req, res) => {
    const userId = res.locals.decoded.userId;
    const sittingId = req.params['sittingId'];

    const sitting = await getRepository(Sitting).findOne(sittingId);


    const convocationRepository = getRepository(Convocation);
    const convocation = await convocationRepository.findOne({sittingId: sittingId, userId: userId})

    const user = await getRepository(User).findOne(userId);

    if (!convocation) {
        res.status(404).send();
        return;
    }

    const attendanceDto = new AttendanceDto()
    attendanceDto.attendance = convocation.attendance;
    attendanceDto.hasDeputy = !!user.deputyId;
    attendanceDto.mandatorId = convocation.mandatorId;
    attendanceDto.availableMandators = await attendanceManager.getAvailableMandators(sittingId, userId);
    attendanceDto.isMandatorAllowed = sitting.isMandatorAllowed
    attendanceDto.isRemoteAllowed = sitting.isRemoteAllowed

    res.send(attendanceDto);

});


attendanceController.post('/sittings/:sittingId', async (req, res) => {
    const userId = res.locals.decoded.userId;
    const sittingId = req.params['sittingId'];
    const attendanceStatus : {attendance: string, mandatorId:string} = req.body.attendanceStatus;

    const result = await attendanceManager.setAttendanceStatus(userId,sittingId, attendanceStatus);

    if(!result) {
        res.status(400).send("error setting attendance status");
        return;
    }

    res.send({success: true});
});

