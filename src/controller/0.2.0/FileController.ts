import * as express from "express";
import logger from "../../service/logger/logger";
import * as fse from 'fs-extra';

import {getCustomRepository, getRepository} from "typeorm";
import {File} from "../../entity/File";
import {FileRepository} from "../../repository/FileRepository";
import {Response} from "express";

export const fileController = express.Router();


fileController.get('/dlPdf/:fileId', async function (req, res) {
    logger.debug("dlPdf", {fileId: req.params["fileId"]});
    let fileId = req.params["fileId"]
    const fileManager = getRepository(File);

    // todo validate fileID is UUID ! peut etre avec joi (pas mal come lib pour faire de la validation)
    const file = await fileManager.findOne(fileId);  // todo peut etre try catcher les requetes quand meme ?
    await sendFile(file, res);

});


fileController.get('/dlAnnexe/:annexId', async function (req, res) {
    logger.debug("dlAnnexe", {annexId: req.params["annexId"]});
    let annexId = req.params["annexId"]
    const fileManager = getCustomRepository(FileRepository);
    const file = await fileManager.getFileFromAnnexId(annexId);

    await sendFile(file, res);

});


// useless ?
fileController.get('/dlInvitation/:id', async function (req, res) {
    logger.debug("dlInvitation", {convocationId: req.params["id"]});
    let convocationId = req.params["id"]

    const fileManager = getCustomRepository(FileRepository);
    const file = await fileManager.getInvitationFileFromConvocationId(convocationId);
    await sendFile(file, res);
});


async function sendFile(file : File | null, res: Response){
    if (!fse.existsSync(file?.path)) {
        logger.error("dlPdf error 404 not found :  fileId => " + file?.id );
        res.status(404).send();
        return;
    }

    try {
        await fse.access(file.path);
    } catch (e) {
        logger.error("dlPdf error 500 error permissions for : ", {
            documentId: file.id,
            file: file.path,
        });
        return res.status(500).send()
    }

    res.setHeader('Content-type', 'application/pdf');
    return res.sendFile(file.path);
}
