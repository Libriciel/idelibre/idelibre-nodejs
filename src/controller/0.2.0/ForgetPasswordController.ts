import * as express from "express";
import axios from "axios";
import * as https from "https";
import config from "../../../config";

export const forgetPasswordController = express.Router();


forgetPasswordController.post('/', async (req, res) => {
    let usernameWithoutSuffix = req.body.username;
    let suffix = req.body.suffix;

    if (!usernameWithoutSuffix || !suffix) {
        return res.status(400).send({message: 'username and suffix are required'})
    }

    let data = {username: `${usernameWithoutSuffix}@${suffix}`}

    const agent = new https.Agent({
        rejectUnauthorized: false
    });

    axios.post(config.nginxIdelibre + '/forgetPasswordJson', data, {httpsAgent: agent})
        .then(() => res.status(200).send({message: 'Reset Password email if exists'}))
        .catch(error => res.status(error.response.status).send({message: error.response.statusText}));
});
