import * as express from "express";
import {socketManager} from "../../service/SocketManager";
import config from "../../../config";
import {nsp20} from "../../socketIo/0.2.0/SocketNamespace";
import logger from "../../service/logger/logger";

export const notificationController = express.Router();

const sendNotification = (passphrase, userIds, eventName, res) => {

    if (passphrase !== config.passphrase) {
        res.status(403).send("wrong passphrase")
        return;
    }
    let nsp = nsp20.getNamespace();
    for (let userId of userIds) {

        const socketId = socketManager.getSocketByUserId(userId)
        if (socketId && nsp) {
            nsp.to(socketId).emit(eventName, {message: eventName});
        }
    }

    res.sendStatus(204);
}


//req.body {passphrase: string, userIds:string[]}
notificationController.post('/sittings/new', (req, res) => {
    logger.debug("push new sitting from server");
    sendNotification(req.body.passphrase, req.body['userIds'] ?? [], 'newSeance', res);
});

//req.body {passphrase: string, userIds:string[]}
notificationController.post('/sittings/modify', (req, res) => {
    logger.debug("push modify sitting from server");
    sendNotification(req.body['passphrase'], req.body['userIds'] ?? [], 'modifiedSeance', res);
})

//req.body {passphrase: string, userIds:string[]}
notificationController.post('/sittings/remove', (req, res) => {
    logger.debug("push remove sitting from server");
    sendNotification(req.body['passphrase'], req.body['userIds'] ?? [], 'removeSeance', res);
})


