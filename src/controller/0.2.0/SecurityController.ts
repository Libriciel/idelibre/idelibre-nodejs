import * as express from "express";
import logger from "../../service/logger/logger";
import axios from "axios";
import config from "../../../config";
import * as https from "https";
import Config from "../../../config";

export const securityController = express.Router();


const changePasswordErrors = {
    'BAD_USER_ID': 'Utilisateur inconnu. ',
    'INVALID_CURRENT_PASSWORD': 'La saisie de votre mot de passe actuel est incorrecte. ',
    'ENTROPY_TOO_LOW': 'Le mot de passe n\'est pas assez fort. '
}

function getErrorMessage(msgCode: string | null): string {
    if (!msgCode || !changePasswordErrors[msgCode]) {
        return "Une erreur est survenue"
    }

    return changePasswordErrors[msgCode];
}


securityController.post('/', async function (req, res) {
    let currentPlainPassword = req.body.currentPassword
    let newPlainPassword = req.body.newPassword
    let userId = res.locals.decoded.userId;


    if (!currentPlainPassword || !newPlainPassword) {
        logger.debug("missing current password or new password field");

        return res.status(500).send();
    }

    const agent = new https.Agent({
        rejectUnauthorized: false
    });
    const data = {
        userId: userId,
        plainNewPassword: newPlainPassword,
        plainCurrentPassword: currentPlainPassword,
        passPhrase: Config.passphrase
    }


    try {
        await axios.post(config.nginxIdelibre + '/security/changePassword', data, {httpsAgent: agent})
    } catch (error) {
        return res.status(400)
            .send({
                message: getErrorMessage(error.response?.data?.message),
                minEntropyValue: error.response?.data?.minEntropyValue,
                currentEntropyValue: error.response?.data?.currentEntropyValue
            });
    }


    res.status(200).send({message: 'passwordChanged', "success" : true})

});

