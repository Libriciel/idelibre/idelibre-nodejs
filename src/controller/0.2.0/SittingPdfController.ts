import * as express from "express";
import {Response} from "express";
import logger from "../../service/logger/logger";
import {getRepository} from "typeorm";
import {Sitting} from "../../entity/Sitting";
import config from "../../../config";
import * as fse from "fs-extra";

export const sittingPdfController = express.Router();


sittingPdfController.get('/dlPdf/:sittingId', async function (req, res) {
    logger.debug("dlFullPdf", {seanceId: req.params["sittingId"]});
    const pdfPath = await getPdfFilePath(req.params["sittingId"])
    await sendFile(pdfPath, res);
});


async function getPdfFilePath(sittingId: string) {
    const sittingRepository = getRepository(Sitting);
    const sitting = await sittingRepository.findOne(sittingId);
    const structureId = sitting?.structureId;
    if (!structureId) {
        return null;
    }
    return config.fullPdfDirectory + structureId + '/' + sittingId + '.pdf';
}


async function sendFile(filePath: string, res: Response) {
    if (!fse.existsSync(filePath)) {
        logger.error("Pdf error 404 not found : ", {pdfFilePath: filePath});
        res.status(404).send();
        return;
    }

    try {
        await fse.access(filePath);
    } catch (e) {
        logger.error("Pdf error 500 error permissions for : ", {
            filePath: filePath,
        });
        return res.status(500).send()
    }
    res.header('Content-type', 'application/pdf');
    return res.sendFile(filePath);
}
