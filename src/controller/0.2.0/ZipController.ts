import * as express from "express";
import {Response} from "express";
import logger from "../../service/logger/logger";
import {getRepository} from "typeorm";
import {Sitting} from "../../entity/Sitting";
import config from "../../../config";
import * as fse from "fs-extra";

export const zipController = express.Router();

zipController.get('/dlZip/:sittingId', async function (req, res) {
    logger.debug("dlZip", {seanceId: req.params["sittingId"]});
    const zipPath = await getZipFilePath(req.params["sittingId"]);
    await sendFile(zipPath, res);

});


async function sendFile(filePath: string, res: Response) {
    if (!fse.existsSync(filePath)) {
        logger.error("Zip error 404 not found : ", {filePath: filePath});
        res.status(404).send();
        return;
    }

    try {
        await fse.access(filePath);
    } catch (e) {
        logger.error("Zip error 500 error permissions for : ", {
            filePath: filePath,
        });
        return res.status(500).send()
    }
    res.header('Content-type', 'application/zip');
    return res.sendFile(filePath);
}


async function getZipFilePath(sittingId: string) {
    const sittingRepository = getRepository(Sitting);
    const sitting = await sittingRepository.findOne(sittingId);
    const structureId = sitting?.structureId;
    if (!structureId) {
        return null;
    }
    return config.zipDirectory + structureId + '/' + sittingId + '.zip';

}
