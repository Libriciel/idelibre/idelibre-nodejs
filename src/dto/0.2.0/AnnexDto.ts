import {Expose, Type} from "class-transformer";
import {FileAnnexDto} from "./FileAnnexDto";

export class AnnexDto {

    @Expose({name: "annexe_id"})
    id: string;

    @Type(() => FileAnnexDto)
    file: FileAnnexDto;

    @Expose({name: "annexe_name"})
    getFileName() :string {
        return this.file.name;
    }

    fileId: string;


    @Expose({name: 'annexe_rank'})
    rank: number;


}
