import {AnnotationUser} from "../../entity/AnnotationUser";
import {Expose, Type} from "class-transformer";
import {User} from "../../entity/User";



class Rect {
  @Expose()
  top: number;
  @Expose()
  left: number;
  @Expose()
  bottom: number;
  @Expose()
  right: number;
}


export class AnnotationDto {

  public currentUserId: string

  @Expose({name: 'annotation_id'})
  id: string;

  @Expose({name: 'annotation_author_id'})
  authorId;

  @Type(() => User)
  author: User

  @Expose({name: 'annotation_author_name'})
  getAuthorName(): string {
    return this.author?.lastName
  }

  @Expose({name: 'annotation_annexe_id'})
  annexId: string;

  @Expose({name: 'annotation_projet_id'})
  projectId: string;

  @Expose({name: 'annotation_seance_id'})
  sittingId: string;

  @Expose({name: 'annotation_page'})
  page: number;

  @Expose({name: 'annotation_rect'})
  @Type(() => Rect)
  rect: Object;

  @Expose({name: 'annotation_text'})
  text: string

  createdAt: Date;

  @Expose({name: 'annotation_date'})
  getDateInMilliseconds(): number {
    return this.createdAt.getTime();
  }


  @Expose({name: 'annotation_shareduseridlist'})
  getRecipientIdList() {
    return this.annotationUsers.map((annotationUser) => annotationUser.userId);
  }

  @Expose({name: 'isread'})
  getIsRead() {
    if (this.currentUserId === this.authorId) {
      return true
    }
    const currentUserAnnotationUser = this.annotationUsers.filter((annotationUser) => annotationUser.userId === this.currentUserId);

    return currentUserAnnotationUser[0]?.isRead ?? false
  }

  @Type(() => AnnotationUser)
  annotationUsers: AnnotationUser[];
}


