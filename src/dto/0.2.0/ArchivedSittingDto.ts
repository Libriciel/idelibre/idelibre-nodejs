import {Expose} from "class-transformer";

export class ArchivedSittingDto {
    @Expose({name: 'seance_id'})
    id: string;

    @Expose({name: 'seance_name'})
    name: string;

    date: Date;

    @Expose({name: 'seance_date'})
    getDateInMilliseconds(): string {
        return "" + this.date.getTime();
    }

    @Expose({name: 'seance_document_id'})
    convocationFileId: string | null;
}
