import {Expose, Type} from "class-transformer";
import {AttendanceEnum} from "../../AttendanceEnum";
import {MandatorDto} from "./MandatorDto";


export class AttendanceDto {

    @Expose({name: 'attendance'})
    attendance: string;

    @Expose({name: 'hasDeputy'})
    hasDeputy: boolean;

    @Expose({name: 'mandatorId'})
    mandatorId: string;

    @Expose({name: 'isMandatorAllowed'})
    isMandatorAllowed: boolean;

    @Expose({name: 'availableMandators'})
    availableMandators: MandatorDto[];

    @Expose({name: 'isRemoteAllowed'})
    isRemoteAllowed: boolean;
}
