import {Expose, Type} from "class-transformer";
import {SittingDto} from "./SittingDto";


export class ConvocationDto {

    @Expose({name: 'convocation_id'})
    id: string;

    @Expose({name: 'convocation_read'})
    isRead: boolean;

    @Expose({name: 'presentStatus'})
    attendance: string | null;

    @Type(() => SittingDto)
    sitting: SittingDto;


    // legacy 4.2
    @Expose({name: 'deputy'})
    deputyLegacy: string | null = null;


    //envoyer le suppleant si il y en a un de set
    //envoyer le suppleant si l'utilisaeur en a un


    //envoyer le mandataire si il y en a un de set
    //envoyer la liste des mandataires potentiels
    //mandatorCandidates


    @Expose({name: 'document_id'})
    getAssociatedFile(): string | null {
        if (this.category === 'convocation') {
            return this.sitting.convocationFileId
        }
        return this.sitting.invitationFileId;
    }

    category: string;

}
