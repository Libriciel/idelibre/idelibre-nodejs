import {Expose, Type} from "class-transformer";
import {SittingDto} from "./SittingDto";


export class InvitationDto {

    @Expose({name: 'invitation_id'})
    id: string;

    @Expose({name: 'invitation_read'})
    isRead: boolean;

    @Expose({name: 'presentStatus'})
    attendance: string | null;

    @Expose({name: 'deputy'})
    deputy: string | null = null;

    @Type(() => SittingDto)
    sitting: SittingDto;



    @Expose({name: 'document_id'})
    getAssociatedFile(): string | null {
        if (this.category === 'convocation') {
            return this.sitting.convocationFileId
        }
        return this.sitting.invitationFileId;
    }

    @Expose({name: 'invitation_document_id'})
    getInvitationDocumentId(): string | null {
        return this.getAssociatedFile();
    }

    category: string;

}
