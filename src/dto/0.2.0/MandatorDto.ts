import {Expose, Type} from "class-transformer";


export class MandatorDto {

    @Expose({name: 'id'})
    id: string;

    @Expose({name: 'firstName'})
    firstName: string;

    @Expose({name: 'lastName'})
    lastName: string;

    @Expose({name: 'username'})
    username: string;
}
