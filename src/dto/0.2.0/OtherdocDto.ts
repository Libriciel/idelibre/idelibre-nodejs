import {Expose, Type} from "class-transformer";


export class OtherdocDto {

    @Expose({name: 'otherdoc_id'})
    id: string;

    @Expose({name: 'otherdoc_document_id'})
    fileId: string;

    @Expose({name: 'otherdoc_name'})
    name: string;

    @Expose({name: 'otherdoc_rank'})
    rank: number;
}
