import {Expose, Type} from "class-transformer";
import {ReporterDto} from "./ReporterDto";
import {ThemeDto} from "./themeDto";
import {AnnexDto} from "./AnnexDto";


export class ProjectDto {

    @Expose({name: 'projet_id'})
    id: string;

    @Expose({name: 'projet_document_id'})
    fileId: string;

    @Expose({name: 'projet_user_id'})
    reporterId: string | null;

    @Expose({name: 'projet_user_name'})
    getReporterName(): string | null {
        if (this.reporter) {
            return this.reporter.firstName + ' ' + this.reporter.lastName
        }
        return null;
    }

    @Expose({name: 'projet_name'})
    name: string;

    @Expose({name: 'projet_rank'})
    rank: number;

    @Type(() => ReporterDto)
    @Expose({name: 'user'})
    reporter: ReporterDto;

    @Type(() => ThemeDto)
    @Expose({name : 'ptheme'})
    theme: ThemeDto

    @Expose({name : 'ptheme_name'})
    getThemeName() : string | null {
        if(!this.theme) {
            return "Sans thème"
        }
        return this.theme.fullName

    }

    @Type(() => AnnexDto)
    @Expose({name: 'annexes'})
    annexes: AnnexDto[];


}
