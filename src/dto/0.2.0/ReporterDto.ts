import {Expose} from "class-transformer";

export class ReporterDto {

    username: string;

    @Expose({name: 'projet_user_firstname'})
    firstName: string;

    @Expose({name: 'projet_user_lastname'})
    lastName: string;

}
