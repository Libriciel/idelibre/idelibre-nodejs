import {Expose, Type} from "class-transformer";
import {ConvocationDto} from "./ConvocationDto";
import {ProjectDto} from "./ProjectDto";
import {StructureDto} from "./StructureDto";
import {OtherdocDto} from "./OtherdocDto";

export class SittingDto {
    @Expose({name: 'seance_id'})
    id: string;

    @Expose({name: 'seance_name'})
    name: string;

    date: Date;

    @Expose({name: 'seance_date'})
    getDateInMilliseconds(): string {
        return "" + this.date.getTime();

    }

    revision: number;

    @Expose({name: 'seance_rev'})
    getSeanceRevString(): string {
        return "" + this.revision;
    }


    @Expose({name: 'seance_place'})
    place: string | null;


    @Expose({name: 'projets'})
    @Type(() => ProjectDto)
    projects: ProjectDto[]

    @Type(() => ConvocationDto)
    convocations: ConvocationDto[]

    @Expose({name: 'convocation'})
    getConvocation(): ConvocationDto {
        return this.convocations[0];
    }

    @Expose({name: 'presentStatus'})
    getPresentStatus(): string | null {
        return this.convocations[0].attendance;
    }
    @Expose({name: 'deputy'})
    getDeputy(): string | null {
        return null;
    }

    convocationFileId: string | null;
    invitationFileId: string | null;

    @Expose({name: 'seance_document_id'})
    getAssociatedFile(): string | null {
        if (this.getConvocation().category === 'convocation') {
            return this.convocationFileId
        }
        return this.invitationFileId;
    }

    @Type(() => StructureDto)
    structure: StructureDto

    @Expose({name: 'otherdocs'})
    @Type(() => OtherdocDto)
    otherdocs: OtherdocDto[]

    @Expose({name: 'isRemoteAllowed'})
    isRemoteAllowed: boolean;
}
