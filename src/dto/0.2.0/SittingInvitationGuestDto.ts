import {Expose, Type} from "class-transformer";
import {InvitationDto} from "./InvitationDto";

export class SittingInvitationGuestDto {
    @Expose({name: 'seance_id'})
    id: string;

    @Expose({name: 'seance_name'})
    name: string;

    date: Date;

    @Expose({name: 'seance_date'})
    getDateInMilliseconds(): string {
        return "" + this.date.getTime();
    }

    revision: number;

    @Expose({name: 'seance_rev'})
    getSeanceRevString():string {
        return "" + this.revision;
    }

    @Expose({name: 'seance_place'})
    place: string | null;

    @Type(() => InvitationDto)
    convocations: InvitationDto[]

    @Expose({name: 'invitation'})
    getConvocation(): InvitationDto {
        return this.convocations[0];
    }

    @Expose({name: 'presentStatus'})
    getPresentStatus(): string | null {
        return this.convocations[0].attendance;
    }


    convocationFileId: string | null;
    invitationFileId: string | null;

    @Expose({name: 'seance_document_id'})
    getAssociatedFile(): string | null {
        if (this.getConvocation().category === 'convocation') {
            return this.convocationFileId
        }
        return this.invitationFileId;
    }


}
