import {Type} from "class-transformer";
import {TimezoneDto} from "./TimezoneDto";

export class StructureDto {

    name: string;

    @Type(() => TimezoneDto)
    timezone: TimezoneDto;


}
