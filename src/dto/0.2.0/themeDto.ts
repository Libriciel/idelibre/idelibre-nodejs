import {Expose} from "class-transformer";

export class ThemeDto{

    @Expose({name: "ptheme_name"})
    fullName: string | null

}
