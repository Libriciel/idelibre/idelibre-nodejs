import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToOne,
} from "typeorm";
import { File } from "./File";
import { Project } from "./Project";

@Index( ["fileId"], { unique: true })
@Index( ["id"], { unique: true })
@Index( ["projectId"], {})
@Entity("annex", { schema: "public" })
export class Annex {
  @Column("uuid", { primary: true, name: "id" })
  id: string;

  @Column("uuid", { name: "file_id" })
  fileId: string;

  @Column("uuid", { name: "project_id" })
  projectId: string;

  @Column("integer", { name: "rank" })
  rank: number;

  @OneToOne(() => File, (file) => file.annex)
  @JoinColumn([{ name: "file_id", referencedColumnName: "id" }])
  file: File;

  @ManyToOne(() => Project, (project) => project.annexes, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{ name: "project_id", referencedColumnName: "id" }])
  project: Project;
}
