import {Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany,} from "typeorm";
import {Project} from "./Project";
import {Sitting} from "./Sitting";
import {Annex} from "./Annex";
import {User} from "./User";
import {AnnotationUser} from "./AnnotationUser";


@Entity("annotation", { schema: "public" })
export class Annotation {
  @Column("uuid", { primary: true, name: "id" })
  id: string;

  @Column("uuid", { name: "author_id" })
  authorId: string;

  @ManyToOne(() => User, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{ name: "author_id", referencedColumnName: "id" }])
  author: User;



  @Column("uuid", { name: "annex_id" , nullable: true})
  annexId: string;

  @ManyToOne(() => Annex, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{ name: "annex_id", referencedColumnName: "id" }])
  annex: Annex;



  @Column("uuid", { name: "project_id", nullable: true })
  projectId: string;

  @ManyToOne(() => Project, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{ name: "project_id", referencedColumnName: "id" }])
  project: Project;



  @Column("uuid", { name: "sitting_id" , nullable: true})
  sittingId: string;

  @ManyToOne(() => Sitting, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{ name: "sitting_id", referencedColumnName: "id" }])
  sitting: Sitting;

  @Column("integer", { name: "page" })
  page: number;

  @Column("jsonb", { name: "rect" })
  rect: object;

  @Column("text", {name: "text", nullable: true})
  text:string

  @Column("timestamp without time zone", { name: "created_at" })
  createdAt: Date;


  @OneToMany(() => AnnotationUser, (annotationUser) => annotationUser.annotation)
  annotationUsers: AnnotationUser[]

}
