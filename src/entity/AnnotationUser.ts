import {Column, Entity, JoinColumn, ManyToOne,} from "typeorm";
import {User} from "./User";
import {Annotation} from "./Annotation";


@Entity("annotation_user", { schema: "public" })
export class AnnotationUser {
  @Column("uuid", { primary: true, name: "id" })
  id: string;


  @Column("uuid", { name: "annotation_id" })
  annotationId: string

  @ManyToOne(() => Annotation, (annotation) => annotation.annotationUsers , {
    onDelete: "CASCADE", cascade: true, onUpdate:"CASCADE"
  })
  @JoinColumn([{ name: "annotation_id", referencedColumnName: "id" }])
  annotation: Annotation;


  @Column("uuid", { name: "user_id" })
  userId: string

  @ManyToOne(() => User, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{ name: "user_id", referencedColumnName: "id" }])
  user: User;


  @Column("boolean", { name: "is_read" , nullable: true})
  isRead: boolean = false;



}
