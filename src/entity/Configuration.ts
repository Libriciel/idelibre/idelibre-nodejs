import {Column, Entity, Index, JoinColumn, OneToOne,} from "typeorm";
import {Structure} from "./Structure";

@Index("Configuration_pkey", ["id"], {unique: true})
@Index(["structureId"], {})
@Entity("configuration", {schema: "public"})
export class Configuration {
    @Column("uuid", {primary: true, name: "id"})
    id: string;

    @Column("boolean", {name: "is_shared_annotation"})
    isSharedAnnotation: boolean;

    @Column("integer", {name: "minimum_entropy"})
    minimumEntropy: number = 80;

    @Column("uuid", {name: "structure_id"})
    structureId: string;

    @OneToOne(() => Structure, (structure) => structure.configuration, {
        onDelete: "CASCADE",
    })
    @JoinColumn([{name: "structure_id", referencedColumnName: "id"}])
    structure: Structure;

}
