import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { Structure } from "./Structure";

@Index("connector_pkey", ["id"], { unique: true })
@Index( ["structureId"], {})
@Entity("connector", { schema: "public" })
export class Connector {
  @Column("uuid", { primary: true, name: "id" })
  id: string;

  @Column("uuid", { name: "structure_id" })
  structureId: string;

  @Column("character varying", { name: "name", length: 255 })
  name: string;

  @Column("jsonb", { name: "fields" })
  fields: object;

  @Column("character varying", { name: "dtype", length: 255 })
  dtype: string;

  @ManyToOne(() => Structure, (structure) => structure.connectors)
  @JoinColumn([{ name: "structure_id", referencedColumnName: "id" }])
  structure: Structure;
}
