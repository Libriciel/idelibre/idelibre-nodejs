import {
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToOne,
} from "typeorm";
import {Timestamp} from "./Timestamp";
import {Sitting} from "./Sitting";
import {User} from "./User";

@Index("convocation_pkey", ["id"], {unique: true})
@Index(["receivedTimestampId"], {unique: true})
@Index(["sentTimestampId"], {})
@Index(["sittingId"], {})
@Index(["userId"], {})
@Entity("convocation", {schema: "public"})
export class Convocation {
    @Column("uuid", {primary: true, name: "id"})
    id: string;

    @Column("uuid", {name: "sitting_id"})
    sittingId: string;

    @Column("uuid", {name: "user_id"})
    userId: string;

    @Column("uuid", {name: "sent_timestamp_id", nullable: true})
    sentTimestampId: string | null;

    @Column("uuid", {name: "received_timestamp_id", nullable: true})
    receivedTimestampId: string | null;

    @Column("boolean", {name: "is_read"})
    isRead: boolean;

    @Column("timestamp without time zone", {name: "created_at"})
    createdAt: Date;

    @Column("boolean", {name: "is_active"})
    isActive: boolean;

    @Column("boolean", {name: "is_emailed", nullable: true})
    isEmailed: boolean | null;

    @Column("character varying", {name: "category", length: 255})
    category: string;

    @Column("character varying", {
        name: "attendance",
        nullable: true,
        length: 255,
        default: () => "NULL::character varying",
    })
    attendance: string | null;


    @OneToOne(() => User, {
        onDelete: "SET NULL", cascade: true
    })
    @JoinColumn([{name: "deputy_id", referencedColumnName: "id"}])
    deputy: User;

    @Column("uuid", {name: "deputy_id", nullable: true})
    deputyId: string | null;


    @OneToOne(() => User, {
        onDelete: "SET NULL", cascade: true
    })
    @JoinColumn([{name: "mandator_id", referencedColumnName: "id"}])
    mandator: User;

    @Column("uuid", {name: "mandator_id", nullable: true})
    mandatorId: string | null;



    @OneToOne(() => Timestamp, (timestamp) => timestamp.convocation, {
        onDelete: "SET NULL", cascade: true
    })
    @JoinColumn([{name: "received_timestamp_id", referencedColumnName: "id"}])
    receivedTimestamp: Timestamp;

    @ManyToOne(() => Timestamp, (timestamp) => timestamp.convocations, {
        onDelete: "SET NULL",
    })
    @JoinColumn([{name: "sent_timestamp_id", referencedColumnName: "id"}])
    sentTimestamp: Timestamp;

    @ManyToOne(() => Sitting, (sitting) => sitting.convocations, {
        onDelete: "CASCADE",
    })
    @JoinColumn([{name: "sitting_id", referencedColumnName: "id"}])
    sitting: Sitting;

    @ManyToOne(() => User, (user) => user.convocations)
    @JoinColumn([{name: "user_id", referencedColumnName: "id"}])
    user: User;


}
