import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToOne,
} from "typeorm";
import { Structure } from "./Structure";
import { Type } from "./Type";

@Index("email_template_pkey", ["id"], { unique: true })
@Index("idx_email_name_structure", ["name", "structureId"], { unique: true })
@Index( ["structureId"], {})
@Index( ["typeId"], { unique: true })
@Entity("email_template", { schema: "public" })
export class EmailTemplate {
  @Column("uuid", { primary: true, name: "id" })
  id: string;

  @Column("uuid", { name: "structure_id" })
  structureId: string;

  @Column("uuid", { name: "type_id", nullable: true })
  typeId: string | null;

  @Column("character varying", { name: "name", length: 255 })
  name: string;

  @Column("text", { name: "content" })
  content: string;

  @Column("character varying", { name: "subject", length: 255 })
  subject: string;

  @Column("boolean", { name: "is_default" })
  isDefault: boolean;

  @Column("character varying", { name: "category", length: 255 })
  category: string;

  @Column("boolean", { name: "is_attachment" })
  isAttachment: boolean;

  @Column("character varying", {
    name: "format",
    length: 255,
    default: () => "NULL::character varying",
  })
  format: string;

  @ManyToOne(() => Structure, (structure) => structure.emailTemplates, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{ name: "structure_id", referencedColumnName: "id" }])
  structure: Structure;

  @OneToOne(() => Type, (type) => type.emailTemplate, { onDelete: "SET NULL" })
  @JoinColumn([{ name: "type_id", referencedColumnName: "id" }])
  type: Type;
}
