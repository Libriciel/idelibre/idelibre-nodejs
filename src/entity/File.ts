import { Column, Entity, Index, OneToOne } from "typeorm";
import { Annex } from "./Annex";
import { Project } from "./Project";
import { Otherdoc } from "./Otherdoc";
import { Sitting } from "./Sitting";
import { v4 as uuidV4 } from 'uuid';

@Index("file_pkey", ["id"], { unique: true })
@Entity("file", { schema: "public" })
export class File {

  constructor() {
    this.id = uuidV4();
  }

  @Column("uuid", { primary: true, name: "id" })
  id: string;

  @Column("character varying", { name: "path", length: 512 })
  path: string;

  @Column("double precision", { name: "size", nullable: true })
  size: number | null;

  @Column("character varying", { name: "name", length: 125 })
  name: string;

  @Column("timestamp without time zone", { name: "created_at" })
  createdAt: Date;

  @OneToOne(() => Annex, (annex) => annex.file)
  annex: Annex;

  @OneToOne(() => Project, (project) => project.file)
  project: Project;

  @OneToOne(() => Sitting, (sitting) => sitting.convocationFile)
  sittingConvocation: Sitting;

  @OneToOne(() => Sitting, (sitting) => sitting.invitationFile)
  sittingInvitation: Sitting;

  @OneToOne(() => Otherdoc, (otherdoc) => otherdoc.file)
  otherdoc: Otherdoc;
}
