import { Column, Entity, Index, JoinColumn, OneToOne } from "typeorm";
import { User } from "./User";

@Index("forget_token_pkey", ["id"], { unique: true })
@Index(["userId"], { unique: true })
@Entity("forget_token", { schema: "public" })
export class ForgetToken {
  @Column("uuid", { primary: true, name: "id" })
  id: string;

  @Column("uuid", { name: "user_id" })
  userId: string;

  @Column("character varying", { name: "token", length: 255 })
  token: string;

  @Column("timestamp without time zone", { name: "expire_at" })
  expireAt: Date;

  @OneToOne(() => User, (user) => user.forgetToken, { onDelete: "CASCADE" })
  @JoinColumn([{ name: "user_id", referencedColumnName: "id" }])
  user: User;
}
