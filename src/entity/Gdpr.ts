import { Column, Entity, Index } from "typeorm";

@Index("gdpr_pkey", ["id"], { unique: true })
@Entity("gdpr", { schema: "public" })
export class Gdpr {
  @Column("uuid", { primary: true, name: "id" })
  id: string;

  @Column("character varying", { name: "company_name", length: 255 })
  companyName: string;

  @Column("character varying", { name: "address", length: 512 })
  address: string;

  @Column("character varying", { name: "representative", length: 255 })
  representative: string;

  @Column("character varying", { name: "quality", length: 255 })
  quality: string;

  @Column("character varying", { name: "siret", length: 255 })
  siret: string;

  @Column("character varying", { name: "ape", length: 255 })
  ape: string;

  @Column("character varying", { name: "company_phone", length: 255 })
  companyPhone: string;

  @Column("character varying", { name: "company_email", length: 255 })
  companyEmail: string;

  @Column("character varying", { name: "dpo_email", length: 255 })
  dpoEmail: string;
}
