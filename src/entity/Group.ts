import { Column, Entity, Index, OneToMany } from "typeorm";
import { Structure } from "./Structure";
import { User } from "./User";

@Index("group_pkey", ["id"], { unique: true })
@Index(["name"], { unique: true })
@Entity("group", { schema: "public" })
export class Group {
  @Column("uuid", { primary: true, name: "id" })
  id: string;

  @Column("character varying", { name: "name", length: 255 })
  name: string;

  @OneToMany(() => Structure, (structure) => structure.group)
  structures: Structure[];

  @OneToMany(() => User, (user) => user.group)
  users: User[];
}
