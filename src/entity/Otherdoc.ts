import {
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToOne,
} from "typeorm";
import { File } from "./File";
import { Sitting } from "./Sitting";

@Index("uniq_otherdoc_file_id", ["fileId"], { unique: true })
@Index("otherdoc_pkey", ["id"], { unique: true })
@Index( ["sittingId"], {})
@Entity("otherdoc", { schema: "public" })
export class Otherdoc {
    @Column("uuid", { primary: true, name: "id" })
    id: string;

    @Column("uuid", { name: "file_id" })
    fileId: string;

    @Column("uuid", { name: "sitting_id" })
    sittingId: string;

    @Column("character varying", { name: "name", length: 512 })
    name: string;

    @Column("integer", { name: "rank" })
    rank: number;

    @Column("timestamp without time zone", { name: "created_at" })
    createdAt: Date;

    @OneToOne(() => File, (file) => file.otherdoc)
    @JoinColumn([{ name: "file_id", referencedColumnName: "id" }])
    file: File;

    @ManyToOne(() => Sitting, (sitting) => sitting.otherdocs, {
        onDelete: "CASCADE",
    })
    @JoinColumn([{ name: "sitting_id", referencedColumnName: "id" }])
    sitting: Sitting;
}
