import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from "typeorm";
import { Structure } from "./Structure";
import { User } from "./User";

@Index("party_pkey", ["id"], { unique: true })
@Index("idx_party_name_structure", ["name", "structureId"], { unique: true })
@Index(["structureId"], {})
@Entity("party", { schema: "public" })
export class Party {
  @Column("uuid", { primary: true, name: "id" })
  id: string;

  @Column("integer", { name: "legacy_id", nullable: false })
  legacyId: number


  @Column("uuid", { name: "structure_id" })
  structureId: string;

  @Column("character varying", { name: "name", length: 255 })
  name: string;

  @ManyToOne(() => Structure, (structure) => structure.parties, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{ name: "structure_id", referencedColumnName: "id" }])
  structure: Structure;

  @OneToMany(() => User, (user) => user.party)
  users: User[];
}
