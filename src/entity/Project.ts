import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
} from "typeorm";
import { Annex } from "./Annex";
import { File } from "./File";
import { User } from "./User";
import { Sitting } from "./Sitting";
import { Theme } from "./Theme";

@Index("uniq_2fb3d0ee93cb796c", ["fileId"], { unique: true })
@Index("project_pkey", ["id"], { unique: true })
@Index(["reporterId"], {})
@Index( ["sittingId"], {})
@Index( ["themeId"], {})
@Entity("project", { schema: "public" })
export class Project {
  @Column("uuid", { primary: true, name: "id" })
  id: string;

  @Column("uuid", { name: "file_id" })
  fileId: string;

  @Column("uuid", { name: "theme_id", nullable: true })
  themeId: string | null;

  @Column("uuid", { name: "reporter_id", nullable: true })
  reporterId: string | null;

  @Column("uuid", { name: "sitting_id" })
  sittingId: string;

  @Column("character varying", { name: "name", length: 512 })
  name: string;

  @Column("integer", { name: "rank" })
  rank: number;

  @Column("timestamp without time zone", { name: "created_at" })
  createdAt: Date;

  @OneToMany(() => Annex, (annex) => annex.project)
  annexes: Annex[];

  @OneToOne(() => File, (file) => file.project)
  @JoinColumn([{ name: "file_id", referencedColumnName: "id" }])
  file: File;

  @ManyToOne(() => User, (user) => user.projects)
  @JoinColumn([{ name: "reporter_id", referencedColumnName: "id" }])
  reporter: User;

  @ManyToOne(() => Sitting, (sitting) => sitting.projects, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{ name: "sitting_id", referencedColumnName: "id" }])
  sitting: Sitting;

  @ManyToOne(() => Theme, (theme) => theme.projects)
  @JoinColumn([{ name: "theme_id", referencedColumnName: "id" }])
  theme: Theme;
}
