import { Column, Entity, Index, OneToMany } from "typeorm";
import { User } from "./User";

@Index("role_pkey", ["id"], { unique: true })
@Index( ["name"], { unique: true })
@Entity("role", { schema: "public" })
export class Role {

  static NAME_ROLE_SECRETARY = 'Secretary';
  static NAME_ROLE_STRUCTURE_ADMINISTRATOR = 'Admin';
  static NAME_ROLE_ACTOR = 'Actor';
  static NAME_ROLE_EMPLOYEE = 'Employee';
  static NAME_ROLE_GUEST = 'Guest';


  @Column("uuid", { primary: true, name: "id" })
  id: string;

  @Column("character varying", { name: "name", length: 255 })
  name: string;

  @Column("jsonb", { name: "composites" })
  composites: object;

  @Column("character varying", { name: "pretty_name", length: 255 })
  prettyName: string;

  @Column("boolean", { name: "is_in_structure_role" })
  isInStructureRole: boolean;

  @OneToMany(() => User, (user) => user.role)
  users: User[];
}
