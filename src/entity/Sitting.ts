import {Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, OneToOne,} from "typeorm";
import {Convocation} from "./Convocation";
import {Project} from "./Project";
import {File} from "./File";
import {Structure} from "./Structure";
import {Type} from "./Type";
import {Otherdoc} from "./Otherdoc";

@Index(["convocationFileId"], {unique: true})
@Index("idx_sitting_name_date_structure", ["date", "name", "structureId"], {
  unique: true,
})
@Index("sitting_pkey", ["id"], {unique: true})
@Index(["invitationFileId"], {unique: true})
@Index(["structureId"], {})
@Index(["typeId"], {})
@Entity("sitting", {schema: "public"})
export class Sitting {
  @Column("uuid", {primary: true, name: "id"})
  id: string;

  @Column("uuid", {name: "type_id", nullable: true})
  typeId: string | null;

  @Column("uuid", {name: "structure_id"})
  structureId: string;

  @Column("uuid", {name: "convocation_file_id", nullable: true})
  convocationFileId: string | null;

  @Column("character varying", {name: "name", length: 255})
  name: string;

  @Column("timestamp without time zone", {name: "date"})
  date: Date;

  @Column("integer", {name: "revision"})
  revision: number;

  @Column("boolean", {name: "is_archived"})
  isArchived: boolean;

  @Column("character varying", {
    name: "place",
    nullable: true,
    length: 255,
    default: () => "NULL::character varying",
  })
  place: string | null;

  @Column("timestamp without time zone", {name: "created_at"})
  createdAt: Date;

  @Column("boolean", { name: "is_mandator_allowed" })
  isMandatorAllowed: boolean = true;

  @Column("uuid", {name: "invitation_file_id", nullable: true})
  invitationFileId: string | null;

  @OneToMany(() => Convocation, (convocation) => convocation.sitting)
  convocations: Convocation[];

  @OneToMany(() => Project, (project) => project.sitting)
  projects: Project[];

  @OneToOne(() => File, (file) => file.sittingConvocation)
  @JoinColumn([{name: "convocation_file_id", referencedColumnName: "id"}])
  convocationFile: File;

  @OneToOne(() => File, (file) => file.sittingInvitation)
  @JoinColumn([{name: "invitation_file_id", referencedColumnName: "id"}])
  invitationFile: File;

  @ManyToOne(() => Structure, (structure) => structure.sittings, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{name: "structure_id", referencedColumnName: "id"}])
  structure: Structure;

  @ManyToOne(() => Type, (type) => type.sittings, {onDelete: "SET NULL"})
  @JoinColumn([{name: "type_id", referencedColumnName: "id"}])
  type: Type;

  @OneToMany(() => Otherdoc, (otherdoc) => otherdoc.sitting)
  otherdocs: Otherdoc[];

  @Column("boolean", {name: "is_remote_allowed"})
  isRemoteAllowed: boolean;
}
