import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany, OneToOne,
} from "typeorm";
import { Connector } from "./Connector";
import { EmailTemplate } from "./EmailTemplate";
import { Party } from "./Party";
import { Sitting } from "./Sitting";
import { Group } from "./Group";
import { Timezone } from "./Timezone";
import { Theme } from "./Theme";
import { Type } from "./Type";
import { User } from "./User";
import {Project} from "./Project";
import {Configuration} from "./Configuration";

@Index( ["groupId"], {})
@Index("structure_pkey", ["id"], { unique: true })
@Index( ["name"], { unique: true })
@Index( ["suffix"], { unique: true })
@Index( ["timezoneId"], {})
@Entity("structure", { schema: "public" })
export class Structure {
  @Column("uuid", { primary: true, name: "id" })
  id: string;

  @Column("uuid", { name: "group_id", nullable: true })
  groupId: string | null;

  @Column("uuid", { name: "timezone_id" })
  timezoneId: string;

  @Column("character varying", { name: "name", length: 255 })
  name: string;

  @Column("character varying", { name: "reply_to", length: 255 })
  replyTo: string;

  @Column("character varying", { name: "suffix", length: 255 })
  suffix: string;

  @Column("boolean", { name: "is_active", nullable: false })
  isActive: boolean | null;

  @Column("character varying", {
    name: "siren",
    nullable: true,
    length: 255,
    default: () => "NULL::character varying",
  })
  siren: string | null;

  @OneToMany(() => Connector, (connector) => connector.structure)
  connectors: Connector[];

  @OneToMany(() => EmailTemplate, (emailTemplate) => emailTemplate.structure)
  emailTemplates: EmailTemplate[];

  @OneToMany(() => Party, (party) => party.structure)
  parties: Party[];

  @OneToMany(() => Sitting, (sitting) => sitting.structure)
  sittings: Sitting[];

  @ManyToOne(() => Group, (group) => group.structures, { onDelete: "SET NULL" })
  @JoinColumn([{ name: "group_id", referencedColumnName: "id" }])
  group: Group;

  @ManyToOne(() => Timezone, (timezone) => timezone.structures)
  @JoinColumn([{ name: "timezone_id", referencedColumnName: "id" }])
  timezone: Timezone;

  @OneToMany(() => Theme, (theme) => theme.structure)
  themes: Theme[];

  @OneToMany(() => Type, (type) => type.structure)
  types: Type[];

  @OneToMany(() => User, (user) => user.structure)
  users: User[];

  @OneToOne(() => Configuration, (configuration) => configuration.structure)
  configuration: Configuration;
}
