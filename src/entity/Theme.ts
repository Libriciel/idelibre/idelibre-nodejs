import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from "typeorm";
import { Project } from "./Project";
import { Structure } from "./Structure";

@Index("theme_pkey", ["id"], { unique: true })
@Index("lft_ix", ["lft"], {})
@Index("lvl_ix", ["lvl"], {})
@Index( ["parentId"], {})
@Index(["rgt"], {})
@Index(["structureId"], {})
@Index(["treeRoot"], {})
@Entity("theme", { schema: "public" })
export class Theme {
  @Column("uuid", { primary: true, name: "id" })
  id: string;

  @Column("uuid", { name: "tree_root", nullable: true })
  treeRoot: string | null;

  @Column("uuid", { name: "parent_id", nullable: true })
  parentId: string | null;

  @Column("uuid", { name: "structure_id" })
  structureId: string;

  @Column("character varying", { name: "name", length: 255 })
  name: string;

  @Column("integer", { name: "lft" })
  lft: number;

  @Column("integer", { name: "lvl" })
  lvl: number;

  @Column("integer", { name: "rgt" })
  rgt: number;

  @Column("character varying", {
    name: "full_name",
    nullable: true,
    length: 512,
    default: () => "NULL::character varying",
  })
  fullName: string | null;

  @OneToMany(() => Project, (project) => project.theme)
  projects: Project[];

  @ManyToOne(() => Theme, (theme) => theme.themes, { onDelete: "CASCADE" })
  @JoinColumn([{ name: "parent_id", referencedColumnName: "id" }])
  parent: Theme;

  @OneToMany(() => Theme, (theme) => theme.parent)
  themes: Theme[];

  @ManyToOne(() => Structure, (structure) => structure.themes, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{ name: "structure_id", referencedColumnName: "id" }])
  structure: Structure;

  @ManyToOne(() => Theme, (theme) => theme.themes2, { onDelete: "CASCADE" })
  @JoinColumn([{ name: "tree_root", referencedColumnName: "id" }])
  treeRoot2: Theme;

  @OneToMany(() => Theme, (theme) => theme.treeRoot2)
  themes2: Theme[];

  setStructure(structure :Structure) {
    this.structure = structure;
  }

  setName(name: string) {
    this.name = name;
  }

  setLft(lft:number) {
    this.lft = lft;
  }

  setRgt(rgt:number) {
    this.rgt = rgt;
  }

  setLvl(lvl:number) {
    this.lvl = lvl
  }

}
