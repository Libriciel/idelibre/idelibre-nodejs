import { Column, Entity, Index, OneToMany, OneToOne } from "typeorm";
import { Convocation } from "./Convocation";
import { v4 as uuidV4 } from 'uuid';

@Index("timestamp_pkey", ["id"], { unique: true })
@Entity("timestamp", { schema: "public" })
export class Timestamp {

  constructor() {
    this.id = uuidV4();
    this.createdAt = new Date();
  }

  @Column("uuid", { primary: true, name: "id" })
  id: string;

  @Column("timestamp without time zone", { name: "created_at" })
  createdAt: Date;

  @Column("character varying", { name: "file_path_content", length: 255 })
  filePathContent: string;

  @Column("character varying", {
    name: "file_path_tsa",
    nullable: true,
    length: 255,
    default: () => "NULL::character varying",
  })
  filePathTsa: string | null;

  @OneToOne(() => Convocation, (convocation) => convocation.receivedTimestamp)
  convocation: Convocation;

  @OneToMany(() => Convocation, (convocation) => convocation.sentTimestamp)
  convocations: Convocation[];
}
