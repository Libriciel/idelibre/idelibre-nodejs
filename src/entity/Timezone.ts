import { Column, Entity, Index, OneToMany } from "typeorm";
import { Structure } from "./Structure";

@Index("timezone_pkey", ["id"], { unique: true })
@Index(["name"], { unique: true })
@Entity("timezone", { schema: "public" })
export class Timezone {
  @Column("uuid", { primary: true, name: "id" })
  id: string;

  @Column("character varying", { name: "name", length: 255 })
  name: string;

  @Column("character varying", {
    name: "info",
    nullable: true,
    length: 255,
    default: () => "NULL::character varying",
  })
  info: string | null;

  @OneToMany(() => Structure, (structure) => structure.timezone)
  structures: Structure[];
}
