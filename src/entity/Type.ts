import {
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
} from "typeorm";
import { EmailTemplate } from "./EmailTemplate";
import { Sitting } from "./Sitting";
import { Structure } from "./Structure";
import { User } from "./User";

@Index("type_pkey", ["id"], { unique: true })
@Index("idx_type_name_structure", ["name", "structureId"], { unique: true })
@Index(["structureId"], {})
@Entity("type", { schema: "public" })
export class Type {
  @Column("uuid", { primary: true, name: "id" })
  id: string;

  @Column("uuid", { name: "structure_id" })
  structureId: string;

  @Column("character varying", { name: "name", length: 255 })
  name: string;

  @OneToOne(() => EmailTemplate, (emailTemplate) => emailTemplate.type)
  emailTemplate: EmailTemplate;

  @OneToMany(() => Sitting, (sitting) => sitting.type)
  sittings: Sitting[];

  @ManyToOne(() => Structure, (structure) => structure.types, {
    onDelete: "CASCADE",
  })
  @JoinColumn([{ name: "structure_id", referencedColumnName: "id" }])
  structure: Structure;

  @ManyToMany(() => User, (user) => user.types)
  @JoinTable({
    name: "type_secretary",
    joinColumns: [{ name: "type_id", referencedColumnName: "id" }],
    inverseJoinColumns: [{ name: "user_id", referencedColumnName: "id" }],
    schema: "public",
  })
  users: User[];

  @ManyToMany(() => User, (user) => user.types2)
  @JoinTable({
    name: "type_user",
    joinColumns: [{ name: "type_id", referencedColumnName: "id" }],
    inverseJoinColumns: [{ name: "user_id", referencedColumnName: "id" }],
    schema: "public",
  })
  users2: User[];
}
