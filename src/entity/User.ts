import {
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToMany,
    ManyToOne,
    OneToMany,
    OneToOne,
} from "typeorm";
import {Convocation} from "./Convocation";
import {ForgetToken} from "./ForgetToken";
import {Project} from "./Project";
import {Type} from "./Type";
import {Group} from "./Group";
import {Party} from "./Party";
import {Role} from "./Role";
import {Structure} from "./Structure";
import {Annotation} from "./Annotation";

@Index(["groupId"], {})
@Index("user_pkey", ["id"], {unique: true})
@Index(["partyId"], {})
@Index(["roleId"], {})
@Index(["structureId"], {})
@Index(["username"], {unique: true})
@Entity("user", {schema: "public"})
export class User {
    @Column("uuid", {primary: true, name: "id"})
    id: string;

    @Column("uuid", {name: "structure_id", nullable: true})
    structureId: string | null;

    @Column("uuid", {name: "group_id", nullable: true})
    groupId: string | null;

    @Column("uuid", {name: "role_id", nullable: true})
    roleId: string | null;

    @Column("uuid", {name: "party_id", nullable: true})
    partyId: string | null;

    @Column("character varying", {name: "username", length: 255})
    username: string;

    @Column("character varying", {name: "email", length: 180})
    email: string;

    @Column("character varying", {name: "password", length: 255})
    password: string;

    @Column("character varying", {name: "first_name", length: 255})
    firstName: string;

    @Column("character varying", {name: "last_name", length: 255})
    lastName: string;

    @Column("character varying", {
        name: "title",
        nullable: true,
        length: 255,
        default: () => "NULL::character varying",
    })
    title: string | null;

    @Column("integer", {name: "gender", nullable: true})
    gender: number | null;

    @Column("boolean", {name: "is_active", nullable: true})
    isActive: boolean | null;

    @Column("timestamp without time zone", {name: "jwt_invalid_before", nullable: true})
    jwtInvalidBefore: Date;


    @OneToMany(() => Convocation, (convocation) => convocation.user)
    convocations: Convocation[];

    @OneToOne(() => ForgetToken, (forgetToken) => forgetToken.user)
    forgetToken: ForgetToken;

    @OneToMany(() => Project, (project) => project.reporter)
    projects: Project[];

    @ManyToMany(() => Type, (type) => type.users)
    types: Type[];

    @ManyToMany(() => Type, (type) => type.users2)
    types2: Type[];

    @ManyToOne(() => Group, (group) => group.users, {onDelete: "CASCADE"})
    @JoinColumn([{name: "group_id", referencedColumnName: "id"}])
    group: Group;

    @ManyToOne(() => Party, (party) => party.users, {onDelete: "SET NULL"})
    @JoinColumn([{name: "party_id", referencedColumnName: "id"}])
    party: Party;

    @ManyToOne(() => Role, (role) => role.users, {onDelete: "SET NULL"})
    @JoinColumn([{name: "role_id", referencedColumnName: "id"}])
    role: Role;

    @ManyToOne(() => Structure, (structure) => structure.users, {
        onDelete: "CASCADE",
    })
    @JoinColumn([{name: "structure_id", referencedColumnName: "id"}])
    structure: Structure;

    setPassword(password: string) {
        this.password = password;
    }

    @OneToOne(() => User, {
        onDelete: "SET NULL", cascade: true
    })
    @JoinColumn([{name: "deputy_id", referencedColumnName: "id"}])
    deputy: User;

    @Column("uuid", {name: "deputy_id", nullable: true})
    deputyId: string | null;


}
