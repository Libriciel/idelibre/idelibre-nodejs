import app from './app'
import {createConnection} from "typeorm";

import {Server} from "socket.io";
import {initSocketIo} from "./socketIo/0.2.0/initSocketIo";
import {nsp20} from "./socketIo/0.2.0/SocketNamespace";




createConnection()
    .then(() => {
        let server = app.listen(3000);
        let io = new Server(server, {cors: {origin: "*"}, allowEIO3: true});
        let namespace020 = initSocketIo(io);
        nsp20.setNamespace(namespace020);

        console.log('server up and running');

    })
    .catch((err) => console.log('connection error : ' + err));




