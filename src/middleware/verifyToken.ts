import {jwtService} from "../service/JwtService";
import logger from "../service/logger/logger";


async function decodeToken(req) {
    if (req.method == 'POST') {
        return await jwtService.verify(req.body?.token)
    }
    return await jwtService.verify(req.headers?.token)
}

export default async function verifyToken(req, res, next) {
    try {
        res.locals.decoded = await decodeToken(req);
        return next();
    } catch (e) {
        logger.error(e);
        res.sendStatus(403);
    }
}
