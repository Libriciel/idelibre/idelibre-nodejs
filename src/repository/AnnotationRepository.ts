import {Convocation} from "../entity/Convocation";
import {EntityRepository, In, Repository} from "typeorm";
import {Annotation} from "../entity/Annotation";


@EntityRepository(Annotation)
export class AnnotationRepository extends Repository<Annotation>{

     getAuthoredAnnotations(userId: string): Promise<Annotation[]> {
         return this
             .createQueryBuilder("annotation")
             .leftJoinAndSelect('annotation.author', 'author')
             .leftJoinAndSelect("annotation.annotationUsers", "annotationUser")
             .where('annotation.authorId = :userId', {userId: userId})
             .getMany();
    }


    getSharedAnnotations(userId: string): Promise<Annotation[]> {
        return this
            .createQueryBuilder("annotation")
            .leftJoinAndSelect('annotation.author', 'author')
            .innerJoinAndSelect("annotation.annotationUsers", "annotationUser")
            .where('annotationUser.userId = :userId', {userId: userId})
            .leftJoinAndSelect('annotation.annotationUsers', 'annotationUserFullList')
            .getMany();
    }

}


