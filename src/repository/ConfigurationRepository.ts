import {Configuration} from "../entity/Configuration";
import {EntityRepository, Repository} from "typeorm";


@EntityRepository(Configuration)
export class ConfigurationRepository extends Repository<Configuration> {

    getConfigurationFromUserId(userId: string): Promise<Configuration> {
        return this
            .createQueryBuilder("configuration")
            .select("configuration.isSharedAnnotation")
            .innerJoin("configuration.structure", "structure")
            .innerJoin("structure.users", "user")
            .where("user.id = :userId", {userId: userId})
            .getOne();
    }
}


