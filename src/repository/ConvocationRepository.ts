import {Convocation} from "../entity/Convocation";
import {EntityRepository, In, Repository} from "typeorm";


@EntityRepository(Convocation)
export class ConvocationRepository extends Repository<Convocation> {

    getActiveConvocationsWithAllDetails(userId: string): Promise<Convocation[]> {
        return this
            .createQueryBuilder("convocation")
            .leftJoinAndSelect("convocation.sitting", "sitting")
            .leftJoinAndSelect("sitting.projects", "project")
            .leftJoinAndSelect("project.annexes", "annex")
            .leftJoinAndSelect("project.theme", "theme")
            .leftJoinAndSelect("project.reporter", "reporter")
            .where("convocation.userId = :userId", {userId: userId})
            .andWhere("convocation.isActive = true")
            .getMany();
    }

    getConvocations(userId: string, sittingsId: string[]): Promise<Convocation[]> {
        return this.find({
            where : {
                sittingId: In(sittingsId),
                userId: userId
            },
            relations: ['sitting', 'user']
        });
    }


}


