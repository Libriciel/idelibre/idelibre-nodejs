import {EntityRepository, Repository} from "typeorm";
import {File} from "../entity/File";

@EntityRepository(File)
export class FileRepository extends Repository<File> {

    getFileFromAnnexId(annexId: string): Promise<File> {
        return this
            .createQueryBuilder('file')
            .innerJoin('file.annex', 'annex')
            .where('annex.id = :annexId', {annexId: annexId})
            .getOne();
    }


    getInvitationFileFromConvocationId(convocationId: string): Promise<File> {
        return this
            .createQueryBuilder('file')
            .innerJoin('file.sittingInvitation', 'sitting')
            .innerJoin('sitting.convocations', 'convocation')
            .where('convocation.id = :convocationId', {convocationId: convocationId})
            .getOne();
    }

}
