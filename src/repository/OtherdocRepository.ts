import {EntityRepository, getRepository, Repository} from "typeorm";

import {Otherdoc} from "../entity/Otherdoc";

@EntityRepository(Otherdoc)
export class OtherdocRepository extends  Repository<Otherdoc>{

    getArchivedOtherdocsFromSitting(sittingId: string): Promise<Otherdoc[]> {
        return getRepository(Otherdoc)
            .createQueryBuilder('otherdoc')
            .innerJoin('otherdoc.sitting', 'sitting')
            .where('sitting.id = :sittingId', {sittingId: sittingId})
            .andWhere('sitting.isArchived = true')
            .orderBy('otherdoc.rank', 'ASC')
            .getMany();
    }



}



