import {EntityRepository, getRepository, Repository} from "typeorm";
import {Project} from "../entity/Project";

@EntityRepository(Project)
export class ProjectRepository extends  Repository<Project>{

    getArchivedProjectsFromSitting(sittingId: string): Promise<Project[]> {
        return getRepository(Project)
            .createQueryBuilder('project')
            .innerJoin('project.sitting', 'sitting')
            .leftJoinAndSelect('project.theme', 'theme')
            .leftJoinAndSelect('project.annexes', 'annex')
            .leftJoinAndSelect('annex.file', 'annexFile')
            .leftJoinAndSelect('project.reporter', 'reporter')
            .where('sitting.id = :sittingId', {sittingId: sittingId})
            .andWhere('sitting.isArchived = true')
            .orderBy('project.rank', 'ASC')
            .addOrderBy('annex.rank', 'ASC')
            .getMany();
    }



}



