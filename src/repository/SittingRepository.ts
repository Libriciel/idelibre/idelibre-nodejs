import {EntityRepository, Repository} from "typeorm";
import {Sitting} from "../entity/Sitting";

@EntityRepository(Sitting)
export class SittingRepository extends Repository<Sitting> {

    getActiveSittingsFromUser(userId: string): Promise<Sitting[]> {
        return this
            .createQueryBuilder("sitting")
            .leftJoinAndSelect('sitting.otherdocs', 'otherdoc')
            .leftJoinAndSelect("sitting.projects", "project")
            .leftJoinAndSelect("project.annexes", "annex")
            .leftJoinAndSelect('annex.file', 'annexFile')
            .leftJoinAndSelect("project.theme", "theme")
            .leftJoinAndSelect("project.reporter", "reporter")
            .innerJoinAndSelect("sitting.convocations", "convocation")
            .leftJoinAndSelect('convocation.sitting', 'sitting.convocation')  //To add both side
            .leftJoinAndSelect("sitting.structure", "structure")
            .leftJoinAndSelect('structure.timezone', 'timezone')
            .where("convocation.userId = :userId", {userId: userId})
            .andWhere("convocation.isActive = true")
            .andWhere("sitting.isArchived = false")
            .orderBy('sitting.date')
            .addOrderBy('project.rank')
            .getMany();
    }


    public getSittingsWithConvocations(sittingIds: String[]): Promise<Sitting[]> {
        return this
            .createQueryBuilder("sitting")
            .leftJoinAndSelect('sitting.convocations', 'convocation')
            .leftJoinAndSelect('convocation.user', 'user')
            .innerJoin('user.role', 'role')
            .andWhere("role.name = 'Actor'")
            .andWhere("sitting.id IN (:...sittingIds)", {sittingIds: sittingIds})

            .orderBy({
                "sitting.date": "ASC",
                "user.firstName": "ASC"
            })
            .getMany();
    }





    getArchivedSittingFromUser(userId: string): Promise<Sitting[]> {
        return this
            .createQueryBuilder("sitting")
            .innerJoin("sitting.convocations", "convocation")
            .where("convocation.userId = :userId", {userId: userId})
            .andWhere("sitting.isArchived = true")
            .getMany();
    }

}



