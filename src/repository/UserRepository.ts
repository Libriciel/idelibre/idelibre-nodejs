import {User} from "../entity/User";
import {EntityRepository, Repository} from "typeorm";
import {Role} from "../entity/Role";

@EntityRepository(User)
export class UserRepository extends Repository<User> {

    getActiveUser(username: string): Promise<User> {
        return this
            .createQueryBuilder('user')
            .leftJoinAndSelect('user.role', 'role')
            .where('user.isActive = true')
            .leftJoinAndSelect('user.structure', 'structure')
            .andWhere('structure.isActive = true')
            .andWhere('user.username = :username', {username: username})
            .getOne();
    }

    getActorFromSitting(sittingId: string): Promise<User[]> {
        return this
            .createQueryBuilder('user')
            .leftJoinAndSelect('user.role', 'role')
            .leftJoinAndSelect('user.party', 'party')
            .innerJoinAndSelect('user.convocations', 'convocation')
            .where('convocation.sittingId = :sittingId', {sittingId: sittingId})
            .andWhere('role.name = :roleName', {roleName: Role.NAME_ROLE_ACTOR})
            .orderBy('user.lastName', 'ASC')
            .addOrderBy('user.firstName', 'ASC')
            .getMany();
    }

    getActorsBySittingId(sittingId: string) {
        return this
            .createQueryBuilder('user')
            .innerJoin('user.role', 'role')
            .andWhere("role.name = 'Actor'")
            .leftJoinAndSelect('user.convocations', 'convocation')
            .andWhere('convocation.sittingId = :sittingId', {sittingId: sittingId})
            .orderBy("user.lastName", "ASC")
            .addOrderBy("user.firstName", "ASC")
            .getMany();
    }
}
