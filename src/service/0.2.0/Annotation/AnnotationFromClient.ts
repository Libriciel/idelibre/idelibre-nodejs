export class AnnotationFromClient {
    annotation_author_id: string;
    annotation_author_name: string;
    annotation_date: number;
    annotation_id: string;
    annotation_page: number;
    annotation_rect: object;
    annotation_shareduseridlist: string[];
    annotation_text: string;
    originType: "Projet" | "Convocation" | 'Annexe';
    originId: string;
}


