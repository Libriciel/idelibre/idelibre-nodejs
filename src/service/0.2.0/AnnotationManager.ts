import {getCustomRepository, getManager, getRepository, In} from "typeorm";
import {AnnotationRepository} from "../../repository/AnnotationRepository";
import {formatAnnotations} from "./formatAnnotations";
import {AnnotationUser} from "../../entity/AnnotationUser";
import {Annotation} from "../../entity/Annotation";
import {User} from "../../entity/User";
import {v4 as uuidV4} from 'uuid';
import {AnnotationFromClient} from "./Annotation/AnnotationFromClient";
import {Configuration} from "../../entity/Configuration";

class AnnotationManager {


    async getAnnotations(userId: string, structureId: string): Promise<any> {
        const annotationRepository = getCustomRepository(AnnotationRepository);
        const authoredAnnotations = await annotationRepository.getAuthoredAnnotations(userId);
        const sharedAnnotations = await this.getSharedAnnotations(userId, structureId);

        const annotations = [...authoredAnnotations, ...sharedAnnotations];

        return formatAnnotations(annotations, userId);
    }

    private async getSharedAnnotations(userId: string, structureId: string): Promise<Annotation[]> {
        const configurationRepository = getRepository(Configuration);
        const configuration = await configurationRepository.findOne({structureId: structureId});

        if (!configuration.isSharedAnnotation) {
            return [];
        }

        const annotationRepository = getCustomRepository(AnnotationRepository);
        return annotationRepository.getSharedAnnotations(userId);
    }


    async setAnnotationToRead(userId: string, annotationIds: string[]): Promise<boolean> {
        if (!annotationIds) {
            return;
        }
        const repository = getRepository(AnnotationUser);
        let annotationUsers = await repository.find({
            annotationId: In(annotationIds),
        });

        annotationUsers.forEach((el) => el.isRead = true);

        return !!await getManager().save(annotationUsers);
    }


    async addOrUpdateAnnotations(annotations: string[], userId: string, isSharedAnnotation: boolean = true): Promise<{ deletedSharedUser: any[], addedSharedUser: any[] }> {
        if (!annotations || annotations.length === 0) {
            return null;
        }

        let deletedSharedUser = [];
        let addedSharedUser = [];

        const annotationRepository = getRepository(Annotation);

        for (let annotationString of annotations) {
            const annotationClient: AnnotationFromClient = JSON.parse(annotationString);
            let annotation = await annotationRepository.findOne({
                where: {id: annotationClient.annotation_id},
                relations: ["annotationUsers"],
            })

            if (annotation) {
                deletedSharedUser.push(...await this.deleteAnnotationAndGetDeletedSharedUsers(annotation));
            }

            await this.insertAnnotation(annotationClient, userId, isSharedAnnotation);

            if (isSharedAnnotation) {
                addedSharedUser.push(...this.getAddedSharedUsers(annotationClient));
            }
        }

        return {
            deletedSharedUser: deletedSharedUser,
            addedSharedUser: addedSharedUser
        }

    }

    async insertAnnotation(annotationClient: AnnotationFromClient, userId: string, isSharedAnnotation: boolean) {
        const annotation = await this.createAnnotationFromAnnotationClient(annotationClient, userId);
        await getManager().save(annotation);

        if (isSharedAnnotation) {
            const annotationUsers = await this.getAnnotationUsers(annotationClient.annotation_shareduseridlist, annotation)
            if (annotationUsers.length) {
                await getManager().save(annotationUsers);
            }
        }
    }


    private async createAnnotationFromAnnotationClient(annotationClient: AnnotationFromClient, userId: string): Promise<Annotation> {
        const annotation = new Annotation();
        annotation.id = annotationClient.annotation_id
        annotation.authorId = userId
        annotation.page = annotationClient.annotation_page
        annotation.createdAt = new Date(annotationClient.annotation_date)
        annotation.rect = annotationClient.annotation_rect;  //WARNING visiblement parfois c'est stringifier parfois non a tester !
        annotation.text = annotationClient.annotation_text ?? "";
        annotation[this.getOriginType(annotationClient)] = annotationClient.originId;

        return annotation;
    }

    private async getAnnotationUsers(sharedUserList: string[], annotation: Annotation): Promise<AnnotationUser[]> {
        if (!sharedUserList || sharedUserList.length === 0) {
            return [];
        }

        const users = await getRepository(User).findByIds(sharedUserList);
        let annotationUsers = []
        for (let user of users) {
            let annotationUser = new AnnotationUser();
            annotationUser.id = uuidV4();
            annotationUser.annotation = annotation;
            annotationUser.user = user;
            annotationUsers.push(annotationUser);
        }

        return annotationUsers;

    }

    private getOriginType(annotationClient: AnnotationFromClient): 'projectId' | 'sittingId' | 'annexId' {

        if (annotationClient.originType === "Projet") {
            return "projectId"
        }
        if (annotationClient.originType === "Convocation") {
            return "sittingId";
        }
        if (annotationClient.originType === "Annexe") {
            return "annexId"
        }
    }


    getAddedSharedUsers(annotationClient: AnnotationFromClient): { userId: string, annotation: AnnotationFromClient }[] {
        let addedSharedUser = []
        if (annotationClient.annotation_shareduseridlist && annotationClient.annotation_shareduseridlist.length > 0) {
            for (let sharedUser of annotationClient.annotation_shareduseridlist) {
                addedSharedUser.push({
                    userId: sharedUser,
                    annotation: annotationClient
                })
            }
        }

        return addedSharedUser;
    }


    private async deleteAnnotationAndGetDeletedSharedUsers(annotation: Annotation): Promise<{ userId: string, annotationId: string }[]> {
        await getRepository(Annotation).delete((annotation.id));
        let deletedSharedUser = [];
        for (const annotationUser of annotation.annotationUsers) {
            deletedSharedUser.push({userId: annotationUser.userId, annotationId: annotation.id});
        }
        return deletedSharedUser;
    }


    async deleteAnnotationByIds(annotationIds: string[], userId: string): Promise<{ userId: string, annotationId: string }[]> {
        if (!annotationIds) {
            return;
        }
        let annotations = await getRepository(Annotation).find({
            where: {id: In(annotationIds), authorId: userId},
            relations: ['annotationUsers']
        });
        let deletedSharedUser = [];
        for (let annotation of annotations) {
            for (let annotationUser of annotation.annotationUsers) {
                deletedSharedUser.push({annotationId: annotation.id, userId: annotationUser.userId});
            }
        }
        await getManager().remove(annotations);
        return deletedSharedUser


    }


}


export const annotationManager = new AnnotationManager();
