import {getCustomRepository, getManager, getRepository} from "typeorm";
import {UserRepository} from "../../repository/UserRepository";
import {MandatorDto} from "../../dto/0.2.0/MandatorDto";
import {Convocation} from "../../entity/Convocation";
import {AttendanceEnum} from "../../AttendanceEnum";
import {User} from "../../entity/User";


class AttendanceManager {


    async getAvailableMandators(sittingId: string, toRemoveUserId: string): Promise<MandatorDto[]> {
        const userRepository = getCustomRepository(UserRepository);
        let actorsInSitting = await userRepository.getActorsBySittingId(sittingId);

        if (toRemoveUserId){
           actorsInSitting = actorsInSitting.filter(user => user.id !== toRemoveUserId);
        }

        const alreadyMandator = actorsInSitting
            .filter(user => user.convocations[0].mandatorId != null)
            .map(user => user.convocations[0].mandatorId);


        const availableMandators = actorsInSitting.filter(user => !alreadyMandator.includes(user.id));



        return availableMandators.map(user => (
            {firstName: user.firstName, lastName: user.lastName, id: user.id, username: user.username})
        );
    }

    async setAttendanceStatus(userId: string, sittingId: string, attendanceStatus: {attendance: string, mandatorId:string}): Promise<boolean> {
        if (!this.isCorrectAttendanceStatus(attendanceStatus.attendance)) {
            return false;
        }

        const convocationRepository = getRepository(Convocation);
        const convocation = await convocationRepository.findOne({sittingId: sittingId, userId: userId})


        if (!convocation) {
            return false;
        }

        convocation.attendance = attendanceStatus.attendance;

        if(attendanceStatus.attendance === AttendanceEnum.AbsentDeputy) {
            const user = await getRepository(User).findOne(userId);
            if(user.deputyId) {
                convocation.deputyId = user.deputyId;
            }else{
                convocation.attendance = AttendanceEnum.Absent;
            }
        }else {
            convocation.deputyId = null;
        }

        if(attendanceStatus.attendance === AttendanceEnum.AbsentMandator) {
            convocation.mandatorId = attendanceStatus.mandatorId;
        }else {
            convocation.mandatorId = null;
        }



        return !!await getManager().save(convocation);
    }

    isCorrectAttendanceStatus(attendanceStatus: string | null): boolean {
        return Object.values(AttendanceEnum).includes(attendanceStatus as AttendanceEnum);
    }

}

export const attendanceManager = new AttendanceManager();
