import {getCustomRepository, getManager, getRepository} from "typeorm";
import {Convocation} from "../../entity/Convocation";
import config from "../../../config";
import {ConvocationRepository} from "../../repository/ConvocationRepository";
import {Timestamp} from "../../entity/Timestamp";
import * as fse from "fs-extra";
import {getLshorodatage} from "../lshorodatage/getLshorodatage";
import {AttendanceEnum} from "../../AttendanceEnum";
import {User} from "../../entity/User";


class ConvocationManager {
    async setAttendanceStatus(userId: string, sittingId: string, attendanceStatus: string, /* legacy 4.2 */ deputy: string | null, mandatorId: string|null = null): Promise<boolean> {
        if (!this.isCorrectAttendanceStatus(attendanceStatus)) {
            return false;
        }

        const convocationRepository = getRepository(Convocation);
        const convocation = await convocationRepository.findOne({sittingId: sittingId, userId: userId})


        if (!convocation) {
            return false;
        }

        convocation.attendance = attendanceStatus;

        if(attendanceStatus === AttendanceEnum.AbsentDeputy) {
            const user = await getRepository(User).findOne(userId);
            if(user.deputyId) {
                convocation.deputyId = user.deputyId;
            }
            convocation.mandatorId = null;
        }

        if(attendanceStatus === AttendanceEnum.AbsentMandator) {
            convocation.mandatorId = mandatorId;
            convocation.deputyId = null;
        }


        return !!await getManager().save(convocation);
    }





    isCorrectAttendanceStatus(attendanceStatus: string | null): boolean {
            return Object.values(AttendanceEnum).includes(attendanceStatus as AttendanceEnum);
    }

    async setRead(userId: string, sittingsId: string[]) {

        if (!sittingsId || !sittingsId.length)
            return true;

        const convocationRepository = getCustomRepository(ConvocationRepository)
        const convocations = await convocationRepository.getConvocations(userId, sittingsId);

        for (let convocation of convocations) {
            if (convocation.isRead) {
                continue;
            }
            convocation.isRead = true;
            convocation.receivedTimestamp = await this.createTimestamp(convocation);
        }

        return !!await getManager().save(convocations);
    }

    private async createTimestamp(convocation: Convocation) {
        let ts = new Timestamp();
        ts.filePathContent = await this.createFileToTimestamp(convocation);
        ts.filePathTsa = await getLshorodatage().timestampFile(ts.filePathContent)
        return ts;
    }

    private async createFileToTimestamp(convocation: Convocation): Promise<string> {

        const year = convocation.sitting.date.getFullYear();
        const fileDir = config.tokenDirectory + convocation.sitting.structureId + '/' + year + '/' + convocation.sittingId;
        await fse.mkdir(fileDir, {recursive: true});

        const filePath = fileDir + '/' + convocation.id;
        let fileCreator = fse.createWriteStream(filePath, {flags: 'a'});
        fileCreator.write("Identifiant de l'utilisateur : " + convocation.user.id + "\n");
        fileCreator.write("Identifiant de la séance : " + convocation.sitting.id + "\n");
        fileCreator.write("Identifiant de la convocation : " + convocation.id + "\n");
        fileCreator.write("Nom de l'utilisateur : " + convocation.user.username + "\n");
        fileCreator.write("Prénom : " + convocation.user.firstName + "\n");
        fileCreator.write("Nom : " + convocation.user.lastName + "\n");
        fileCreator.write("Nom de la séance  : " + convocation.sitting.name + "\n");
        fileCreator.write("Date de la séance  : " + convocation.sitting.date + "\n");
        fileCreator.write("Date d'horodatage  : " + new Date().toISOString() + "\n");
        fileCreator.end();

        return filePath;
    }
}

export const convocationManager = new ConvocationManager();
