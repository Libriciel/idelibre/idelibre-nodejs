import {OtherdocRepository} from "../../repository/OtherdocRepository";
import {getCustomRepository} from "typeorm";
import {formatArchivedOtherdoc} from "./formatOtherdocs";

class OtherdocManager {

    async getArchivedOtherdocsFromSitting(sittingId :string) {
        const otherdocRepository = getCustomRepository(OtherdocRepository);
        let archivedOtherdocs = await otherdocRepository.getArchivedOtherdocsFromSitting(sittingId);
        return formatArchivedOtherdoc(archivedOtherdocs)
    }

}

export const otherdocManager = new OtherdocManager();
