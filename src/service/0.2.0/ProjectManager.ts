import {ProjectRepository} from "../../repository/ProjectRepository";
import {getCustomRepository} from "typeorm";
import {formatArchivedProject} from "./formatProjects";

class ProjectManager {

    async getArchivedProjectsFromSitting(sittingId :string) {
        const projectRepository = getCustomRepository(ProjectRepository);
        let archivedProjects = await projectRepository.getArchivedProjectsFromSitting(sittingId);
        return formatArchivedProject(archivedProjects)
    }

}

export const projectManager = new ProjectManager();
