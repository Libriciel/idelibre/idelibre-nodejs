import {getCustomRepository} from "typeorm";
import {SittingRepository} from "../../repository/SittingRepository";
import {Sitting} from "../../entity/Sitting";
import * as _ from 'lodash'
import {formatArchivedSittings, formatSittingsActor, formatSittingsEmployee, formatSittingsGuest} from "./formatSittings";
import config from "../../../config";


class SittingManager {

    async getFormattedSittings(userId: string, seancesList: { seanceId: string, seanceRev: number }[], group_id: string): Promise<{ toAdd: any, toModify: any, toRemove: any }> {
        let sittings = await this.getActiveSittings(userId);
        return this.updateSeancesStatus(sittings, seancesList, group_id)
    }

    async getFormattedArchivedSittings(userId: string): Promise<any[]> {
        const sittingRepository = getCustomRepository(SittingRepository);
        let archivedSittings = await sittingRepository.getArchivedSittingFromUser(userId);
        return formatArchivedSittings(archivedSittings);
    }


    private getActiveSittings(userId: string): Promise<Sitting[]> {
        const sittingRepository = getCustomRepository(SittingRepository);
        return sittingRepository.getActiveSittingsFromUser(userId)
    }


    private updateSeancesStatus(sittings: Sitting[], seancesList: { seanceId: string, seanceRev: number }[], groupId: string): { toAdd: any, toModify: any, toRemove: any } {
        seancesList = _.uniqBy(seancesList, 'seanceId');

        let toAdd = [];
        let toModify = [];

        for (let sitting of sittings) {
            let indexInSeanceList = this.indexInSeanceList(sitting.id, seancesList)
            if (indexInSeanceList === -1) {
                toAdd.push(sitting);
                continue;
            }
            if (sitting.revision === seancesList[indexInSeanceList].seanceRev) {
                seancesList.splice(indexInSeanceList, 1);
                continue;
            }
            toModify.push(sitting);
            seancesList.splice(indexInSeanceList, 1);
        }

        const formatter = this.getFormatter(groupId);

        return {'toAdd': formatter(toAdd), 'toModify': formatter(toModify), 'toRemove': seancesList}
    }


    private  addMandatorList(sittings: Sitting[]) {
        const sittingIds = sittings.map(sitting => sitting.id);

    }


    private getFormatter(groupId): CallableFunction {
        if (groupId === config.legacy.groupActor) {
            return formatSittingsActor
        }
        if (groupId === config.legacy.groupEmployee) {
            return formatSittingsEmployee
        }

        return formatSittingsGuest
    }


    private indexInSeanceList(seanceId: string, seancesList: { seanceId: string, seanceRev: number }[]): number {
        for (let i = 0; i < seancesList.length; i++) {
            if (seanceId === seancesList[i].seanceId) {
                return i;
            }
        }
        return -1;
    }
}

export const sittingManager = new SittingManager()

