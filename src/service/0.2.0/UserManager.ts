import {getCustomRepository} from "typeorm";
import {UserRepository} from "../../repository/UserRepository";
import {User} from "../../entity/User";

class UserManager {

    async getActorFromSitting(sittingId: string) {
        const userRepository = getCustomRepository(UserRepository);
        const actors = await userRepository.getActorFromSitting(sittingId);
        return await this.formatActor(actors, sittingId);
    }

    async formatActor(actors: User[], sittingId: string): Promise<any[]> {
        let formattedActor = []
        for (let actor of actors) {
            formattedActor.push({
                id: actor.id,
                firstname: actor.firstName,
                lastname: actor.lastName,
                username: actor.username,
                groupepolitique_id : actor.party?.legacyId ?? null,  // Les terminaux attendent un int donc on genere un id en int
                groupepolitique_name: actor.party?.name ?? null,
                seance_id: sittingId
            })
        }

        return formattedActor;
    }


}

export const userManager = new UserManager();
