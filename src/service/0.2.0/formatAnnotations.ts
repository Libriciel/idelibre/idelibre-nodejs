import {classToPlain, plainToClass} from "class-transformer";
import {Annotation} from "../../entity/Annotation";
import {AnnotationDto} from "../../dto/0.2.0/AnnotationDto";

export const formatAnnotations = (annotations: Annotation[], currentUserId: string): any => {
    const annotationDto = plainToClass(AnnotationDto, annotations, {ignoreDecorators: true});
    annotationDto.forEach(el => el.currentUserId = currentUserId);

    return classToPlain(annotationDto, {strategy: 'excludeAll'})
}

