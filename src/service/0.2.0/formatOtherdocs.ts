import {classToPlain, plainToClass} from "class-transformer";
import {Otherdoc} from "../../entity/Otherdoc";
import {OtherdocDto} from "../../dto/0.2.0/OtherdocDto";

export const formatArchivedOtherdoc = (otherdocs : Otherdoc[]): any => {

    const otherdocDto = plainToClass(OtherdocDto, otherdocs, {ignoreDecorators: true});
    return classToPlain(otherdocDto, {strategy: 'excludeAll'})
}

