import {classToPlain, plainToClass} from "class-transformer";
import {Project} from "../../entity/Project";
import {ProjectDto} from "../../dto/0.2.0/ProjectDto";

export const formatArchivedProject = (projects : Project[]): any => {

    const projectDto = plainToClass(ProjectDto, projects, {ignoreDecorators: true});
    return classToPlain(projectDto, {strategy: 'excludeAll'})
}

