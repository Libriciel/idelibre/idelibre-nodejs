import {classToPlain, instanceToPlain, plainToClass} from "class-transformer";
import {SittingDto} from "../../dto/0.2.0/SittingDto";
import {Sitting} from "../../entity/Sitting";
import {SittingInvitationDto} from "../../dto/0.2.0/SittingInvitationDto";
import {SittingInvitationGuestDto} from "../../dto/0.2.0/SittingInvitationGuestDto";
import {ArchivedSittingDto} from "../../dto/0.2.0/ArchivedSittingDto";

export const formatSittingsActor = (sittings : Sitting[]): any => {
    const sittingDto = plainToClass(SittingDto, sittings, {ignoreDecorators: true});
    return instanceToPlain(sittingDto, {strategy: 'excludeAll'})
}


export const formatSittingsEmployee = (sittings : Sitting[]): any => {
    const sittingDto = plainToClass(SittingInvitationDto, sittings, {ignoreDecorators: true});
    return instanceToPlain(sittingDto, {strategy: 'excludeAll'})
}


export const formatSittingsGuest = (sittings : Sitting[]): any => {
    const sittingDto = plainToClass(SittingInvitationGuestDto, sittings, {ignoreDecorators: true});
    return instanceToPlain(sittingDto, {strategy: 'excludeAll'})
}


export const formatArchivedSittings = (sittings : Sitting[]): any => {
    const sittingDto = plainToClass(ArchivedSittingDto, sittings, {ignoreDecorators: true});
    return classToPlain(sittingDto, {strategy: 'excludeAll'})
}
