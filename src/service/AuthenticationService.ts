import * as argon2 from "argon2";
import config from "../../config";
import {UserRepository} from "../repository/UserRepository";
import {User} from "../entity/User";
import {getCustomRepository, getManager} from "typeorm";
import logger from "./logger/logger";
import * as sha1 from "sha1"
import {jwtService} from "./JwtService";
import {ConfigurationRepository} from "../repository/ConfigurationRepository";
import {Configuration} from "../entity/Configuration";

class AuthenticationService {

    async updatePassword(user: User, plainPassword: string): Promise<void> {
        user.password = await argon2.hash(plainPassword)
        await getManager().save(user);
    }

    private async loginLegacy(user: User, plainPassword: string): Promise<{ userId: string, group_id: string, structureId: string } | null> {
        let passwordSalt = config.salt + plainPassword;
        let passwordSha1 = sha1(passwordSalt);
        if (passwordSha1 === user.password) {
            await this.updatePassword(user, plainPassword);
            return {userId: user.id, group_id: this.getLegacyGroup(user), structureId: user.structureId};
        }

        return null;
    }

    private getLegacyGroup(user: User): string | null {
        if (!user.role) {
            return null;
        }

        switch (user.role.name) {
            case 'Actor' :
                return config.legacy.groupActor;
            case 'Employee' :
            case 'Secretary':
            case 'Admin':
                return config.legacy.groupEmployee;
            case 'Guest' :
                return config.legacy.groupGuest;
            default :
                return null;
        }
    }


    async verifyPassword(username: string, plainPassword: string): Promise<{ userId: string, group_id: string, structureId: string } | null> {
        const userRepository = getCustomRepository(UserRepository)
        const user = await userRepository.getActiveUser(username);

        if (!user || !plainPassword) {
            return null;
        }

        try {
            if (await argon2.verify(user.password, plainPassword)) {
                return {userId: user.id, group_id: this.getLegacyGroup(user), structureId: user.structureId};   //todo definir le roleID avec l'existant en v3 !
            }
        } catch (e) {
            logger.error(e);
        }

        return this.loginLegacy(user, plainPassword);
    }


    async authenticateWithToken(jwt: string|null): Promise<{ success: boolean, message:string, token:string, userId:string, group_id:string, groupId:string, configuration:Configuration, isLoginWithTokenAllowed: boolean } | null>
    {
        if(!jwt) {
            return null
        }
        try {
            const decoded = await jwtService.verify(jwt);
            return {
                success:true,
                message: 'token',
                token: jwt,
                userId: decoded.userId,
                group_id: decoded.group_id,
                groupId: decoded.group_id,
                configuration: await getCustomRepository(ConfigurationRepository).getConfigurationFromUserId(decoded.userId),
                isLoginWithTokenAllowed: true
            }

        }catch (e) {
            console.log("bad or expired token")
            return null;
        }
    }

}




export const authenticationService = new AuthenticationService();
