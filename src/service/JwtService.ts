import * as jwt from 'jsonwebtoken'
import config from "../../config";
import {getRepository} from "typeorm";
import {UserRepository} from "../repository/UserRepository";
import {User} from "../entity/User";


class JwtService {

    async sign(content: object): Promise<string> {
        return this.signPromise(content, config.jwtSecret)
    }

    /**
     * @throws
     */
    async verify(token: string): Promise<{ userId: string, group_id: string, iat: number, structureId: string }> {
        let decoded = await this.verifyPromise(token, config.jwtSecret)

        if (await this.isJwtInvalidated(decoded.userId, new Date(decoded.iat * 1000))) {
            throw new InvalidatedTokenError('jwt has been invalidated');
        }

        return decoded;
    }

    async signPromise(content: object, secret: string): Promise<string> {
        return new Promise((resolve, reject) => {
            jwt.sign(content, secret, {algorithm: 'HS256'},
                (error, result) => {
                    if (error) {
                        return reject(error)
                    }
                    resolve(result)
                })
        })
    }

    async verifyPromise(token: string, secret: string): Promise<{ userId: string, group_id: string, iat: number, structureId: string }> {
        return new Promise((resolve, reject) => {
            jwt.verify(token, secret,
                (error, result: any) => {
                    if (error) {
                        return reject(error)
                    }
                    resolve(result)
                })
        })
    }

    async isJwtInvalidated(userId:string, iat:Date): Promise<boolean>
    {
        if(!userId) {
            return true;
        }

        let user = await getRepository(User).findOne(userId);

        if(!user || !user.isActive) {
            return true
        }

        if(!user.jwtInvalidBefore) {
            return false;
        }

        return iat < user.jwtInvalidBefore
    }

}

export const jwtService = new JwtService()


export class InvalidatedTokenError extends Error {
    constructor(message) {
        super(message);
        this.name = "InvalidateTokenError";
    }
}