class SocketManager {

    clientsSocketsList = {};

    getSocketByUserId = function (clientId: string |null): string | null {
        if(!clientId) {
            return null;
        }
        return this.clientsSocketsList[clientId] ?? null;
    };

    associateClientSocket = function (clientId: string, socketId: string) {

        if(!clientId || !socketId) {
            return
        }
        this.clientsSocketsList[clientId] = socketId;
    };

    clear = () => {
        this.clientsSocketsList = {};
    }
}

export const socketManager = new SocketManager()
