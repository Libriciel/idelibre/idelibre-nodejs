import * as winston from "winston";
import config from "../../../config";


const transports = [
    new winston.transports.Console({
        level: config.logLevel,
        handleExceptions: true,
    }),
    new winston.transports.File({
        filename: config.logPath,
        level: config.logLevel,
        handleExceptions: true,
    })
];


const format = winston.format.combine(
    winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    winston.format.printf(
        (info) => `${info.level}: ${info.message}`,
    ),
)


const logger = winston.createLogger({
    transports: transports,
    format: format,
    exitOnError: false
})


if(config.env === 'test') {
    logger.transports.forEach((t) => (t.silent = true));
}

export default logger;



