import {LshorodatageInterface} from "./lshorodatageInterface";
import * as fse from "fs-extra";

export class FakeLshorodatage implements LshorodatageInterface{
    check(): Promise<{ success: boolean }> {
        return Promise.resolve({success: false});
    }

    async timestampFile(filePath: string): Promise<string> {
        await fse.writeFile(filePath + '.tsa', "fake timestamp data");
        return Promise.resolve(filePath + '.tsa');
    }

}
