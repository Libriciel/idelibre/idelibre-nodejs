import axios from 'axios'
import config from "../../../config";
import * as FormData from 'form-data'
import * as fse from "fs-extra";
import logger from "../logger/logger";
import {LshorodatageInterface} from "./lshorodatageInterface";

export class Lshorodatage implements LshorodatageInterface{

     check = async() : Promise<{ success: boolean }> => {
         return await axios.get(config.lshorodatageUrl + '/ping');
     }


     timestampFile = async(filePath :string) : Promise<string> => {
         const form = new FormData();
         form.append('file', fse.createReadStream(filePath));

         try {
             const response = await axios.post(config.lshorodatageUrl + '/timestamp/create', form, {
                 headers: form.getHeaders(),
                 responseType: 'arraybuffer'
             })
             await fse.writeFile(filePath + '.tsa', response.data)
         }catch (e) {
             logger.error(e);
         }
         return filePath + '.tsa';
     }

}

