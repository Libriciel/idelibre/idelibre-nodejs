import config from "../../../config";
import {FakeLshorodatage} from "./FakeLshorodatage";
import {Lshorodatage} from "./Lshorodatage";
import {LshorodatageInterface} from "./lshorodatageInterface";


let lshorodatage: LshorodatageInterface;

export const getLshorodatage = () :LshorodatageInterface => {
    if(!lshorodatage) {
        if (config.env === 'test') {
            lshorodatage = new FakeLshorodatage()
            return lshorodatage;
        }
        lshorodatage = new Lshorodatage();
    }

    return lshorodatage;
}
