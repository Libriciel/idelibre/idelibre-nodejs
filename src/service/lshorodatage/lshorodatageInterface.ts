export interface LshorodatageInterface {
    check() : Promise<{ success: boolean }>;
    timestampFile(filePath: string): Promise<string>;
}
