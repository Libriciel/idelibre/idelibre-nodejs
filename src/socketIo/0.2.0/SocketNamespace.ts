import {Namespace} from "socket.io";

class SocketNamespace {
    private namespace: Namespace

    setNamespace(namespace: Namespace) {
        this.namespace = namespace;
    }

    getNamespace():Namespace | null {
        return this.namespace ?? null;
    }

}

export const nsp20 = new SocketNamespace();
