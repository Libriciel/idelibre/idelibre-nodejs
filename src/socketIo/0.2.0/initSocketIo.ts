import {Namespace, Server, Socket} from "socket.io";
import {jwtService} from "../../service/JwtService";
import {authenticationService} from "../../service/AuthenticationService";
import {sittingManager} from "../../service/0.2.0/SittingManager";
import config from "../../../config";
import {convocationManager} from "../../service/0.2.0/ConvocationManager";
import {projectManager} from "../../service/0.2.0/ProjectManager";
import logger from "../../service/logger/logger";
import {socketManager} from "../../service/SocketManager";
import {userManager} from "../../service/0.2.0/UserManager";
import {annotationManager} from "../../service/0.2.0/AnnotationManager";
import {getCustomRepository, getRepository} from "typeorm";
import {Configuration} from "../../entity/Configuration";
import {ConfigurationRepository} from "../../repository/ConfigurationRepository";
import {Structure} from "../../entity/Structure";
import {otherdocManager} from "../../service/0.2.0/OtherdocManager";
import {isSet} from "lodash";

export const initSocketIo = (io: Server) => {

    async function checkToken(data, socket): Promise<{ userId: string, group_id: string, iat: number, structureId: string }> {
        try {
            return await jwtService.verify(data?.token)
        } catch (e) {
            logger.error(e);
            socket.emit('tokenError', JSON.stringify({success: false, message: 'token error'}));
            return null;
        }
    }


    function notifyDeletedSharedUsers(deletedSharedUsers: any[], nsp: Namespace) {
        for (let deletedSharedUser of deletedSharedUsers) {
            let socketClientId = socketManager.getSocketByUserId(deletedSharedUser.userId);
            if (socketClientId)
                nsp.to(socketClientId).emit('sharedAnnotationDeleted', {annotation_Id: deletedSharedUser.annotationId});
        }
    }

    function notifyAddedSharedUsers(addedSharedUsers: any[], nsp: Namespace) {
        for (let addedSharedUser of addedSharedUsers) {
            let socketClientId = socketManager.getSocketByUserId(addedSharedUser.userId);
            if (socketClientId) {

                /*if (res.toAddSharedUsers[i].annotation.annotation_rect) {
                    res.toAddSharedUsers[i].annotation.annotation_rect = JSON.parse(res.toAddSharedUsers[i].annotation.annotation_rect)
                }*/

                // FIXME android version needs this to be outside the annotation object
                addedSharedUser["originType"] = addedSharedUser.annotation.originType;
                addedSharedUser["originId"] = addedSharedUser.annotation.originId;

                nsp.to(socketClientId).emit('newAnnotation', JSON.stringify(addedSharedUser));
            }
        }
    }


    function sanitizeSeancesList(seancesList: { seanceId: string, seanceRev: number | string }[]): { seanceId: string, seanceRev: number }[] {
        if (!seancesList) {
            return [];
        }

        let sanitizedSeancesList = [];
        for (let seance of seancesList) {
            sanitizedSeancesList.push({seanceId: seance.seanceId, seanceRev: +seance.seanceRev})
        }
        return sanitizedSeancesList;
    }


    const nsp020 = io.of("/0.2.0");
    nsp020.on('connection', (socket: Socket) => {

        socket.emit('hello', {message: "Welcome"});

        socket.on('authenticate', async function (content: { username: string | null, suffix: string | null, password: string | null, token: string | null }) {
            logger.debug(`authenticate : ${content.username} \ ${content.suffix}`);

            if (!content.password || content.password === '') {

                const toSend = await authenticationService.authenticateWithToken(content.token)

                if (!toSend) {
                    socket.emit('authFeedback', {
                        success: false,
                        message: 'Token incorrect',
                        token: null,
                        userId: null,
                        isLoginWithTokenAllowed: true
                    });
                    return;
                }
                socketManager.associateClientSocket(toSend.userId, socket.id);
                socket.emit('authFeedback', toSend)
                return;

            }

            let loggedInUser = await authenticationService.verifyPassword(`${content?.username}@${content?.suffix}`, content?.password);
            if (loggedInUser) {
                socketManager.associateClientSocket(loggedInUser.userId, socket.id);
                const configuration = await getCustomRepository(ConfigurationRepository).getConfigurationFromUserId(loggedInUser.userId);
                socket.emit('authFeedback', {
                    success: true,
                    message: 'token',
                    token: await jwtService.sign(loggedInUser),
                    userId: loggedInUser.userId,
                    group_id: loggedInUser.group_id,
                    groupId: loggedInUser.group_id,
                    configuration: configuration,
                    isLoginWithTokenAllowed: true
                });
                return;
            }
            socket.emit('authFeedback', {success: false, message: '', token: null, userId: null, isLoginWithTokenAllowed: true});
        });


        // Horrible seanceRev peut etre soit un string soit un entier donc on le sanitize
        socket.on('updateSeances', async function (content: { token: string, seancesList: [{ seanceId: string, seanceRev: number | string }] }) {
            logger.debug('updateSeances');
            let decodedToken = await checkToken(content, socket);
            if (!decodedToken) {
                return;
            }

            const updateSittings = await sittingManager.getFormattedSittings(decodedToken.userId, sanitizeSeancesList(content.seancesList), decodedToken.group_id)

            switch (decodedToken.group_id) {
                case config.legacy.groupActor:
                    socket.emit('updateSeancesFeedback', JSON.stringify(updateSittings));
                    break;
                case config.legacy.groupEmployee:
                    socket.emit('updateSeancesAdministratifsFeedback', JSON.stringify(updateSittings));
                    break;
                case config.legacy.groupGuest:
                    socket.emit('updateSeancesInvitesFeedback', JSON.stringify(updateSittings));
                    break;
            }

        });

        socket.on("sendPresence", async function (content: { token: string, seanceId: string, presentStatus: string, procuration_name: string }) {
            logger.debug('sendPresence');
            let decodedToken = await checkToken(content, socket);
            if (!decodedToken)
                return;

            let res = await convocationManager.setAttendanceStatus(decodedToken.userId, content.seanceId, content.presentStatus, content.procuration_name);
            if (res)
                socket.emit("confirmPresenceFeedback", {success: true})
        });


        socket.on('ARs', async function (content: { token: string, seancesId: string[] }) {
            logger.debug('ARs');
            let decodedToken = await checkToken(content, socket);
            if (!decodedToken)
                return;

            let isSuccess = await convocationManager.setRead(decodedToken.userId, content.seancesId);
            if (isSuccess) {
                socket.emit("convocationReadFeedBack", {});
            }
        });


        socket.on('archivedSeancesList', async function (content: { token: string }) {
            logger.debug('archivedSeancesList');
            let decodedToken = await checkToken(content, socket);
            if (!decodedToken)
                return;

            let sittings = await sittingManager.getFormattedArchivedSittings(decodedToken.userId)

            if (sittings)
                socket.emit("archivedSeancesListFeedBack", JSON.stringify(sittings))
        });


        socket.on('archivedProjetsList', async function (content: { token: string, seanceId: string }) {
            logger.debug('archivedProjetsList');
            let decodedToken = await checkToken(content, socket);

            if (!decodedToken)
                return;

            let projects = await projectManager.getArchivedProjectsFromSitting(content.seanceId)

            if (projects)
                socket.emit("archivedProjetsListFeedBack", JSON.stringify(projects))
        });


        socket.on('archivedOtherdocsList', async function (content: { token: string, seanceId: string }) {
            logger.debug('archivedOtherdocsList');
            let decodedToken = await checkToken(content, socket);

            if (!decodedToken)
                return;

            let otherdocs = await otherdocManager.getArchivedOtherdocsFromSitting(content.seanceId)

            if (otherdocs)
                socket.emit("archivedOtherdocsListFeedBack", JSON.stringify(otherdocs))
        });


        socket.on('getUserList', async function (content: { token: string, seanceId: string }) {
            logger.debug('getUserList');
            let decodedToken = await checkToken(content, socket);
            if (!decodedToken)
                return;

            const actors = await userManager.getActorFromSitting(content.seanceId);

            if (actors)
                socket.emit("userList", JSON.stringify(actors));
        });


        socket.on("getAnnotations", async function (content: { token: string }) {
            logger.debug('getAnnotations');
            let decodedToken = await checkToken(content, socket);
            if (!decodedToken)
                return;

            let annotations = await annotationManager.getAnnotations(decodedToken.userId, decodedToken.structureId);

            if (annotations)
                socket.emit("getAnnotationsFeedback", JSON.stringify(annotations));
        });


        socket.on("sendAnnotationRead", async function (content: { token: string, annotationIds: string[] }) {
            logger.debug('sendAnnotationRead');

            let decodedToken = await checkToken(content, socket);
            if (!decodedToken)
                return;

            await annotationManager.setAnnotationToRead(decodedToken.userId, content.annotationIds);

            socket.emit("sendAnnotationReadFeedback", {})
        });


        socket.on("readAnnotationsConnect", async function (content: { token: string, readOfflineList: string[] }) {
            logger.debug('readAnnotationsConnect');
            let decodedToken = await checkToken(content, socket);
            if (!decodedToken)
                return;

            await annotationManager.setAnnotationToRead(decodedToken.userId, content.readOfflineList);

            socket.emit('readAnnotationsConnectFeedback', {});
        });


        socket.on("annotation", async function (content: { token: string, annotation: string[] }) {
            logger.debug('annotation');
            let decodedToken = await checkToken(content, socket);
            if (!decodedToken)
                return;

            const structure = await getRepository(Structure).findOne(decodedToken.structureId, {relations: ['configuration']})
            const toNotify = await annotationManager.addOrUpdateAnnotations(content.annotation, decodedToken.userId, structure.configuration.isSharedAnnotation ?? true);
            socket.emit("addNewAnnotationFeedback", {});

            if (!toNotify) {
                return;
            }

            notifyDeletedSharedUsers(toNotify.deletedSharedUser, nsp020);
            notifyAddedSharedUsers(toNotify.addedSharedUser, nsp020);
        });

        // WARNING n'est plus utilisé que pour le client android android et devrait disparaitre !
        // Donc pas de refacto avec sa jumelle deleteAnnotations
        socket.on("deleteAnnotationsConnect", async function (content: { token: string, deleteOfflineList: string[] }) {
            logger.debug('deleteAnnotationConnect');
            let decoded = await checkToken(content, socket);
            if (!decoded)
                return;

            let sharedUsers = await annotationManager.deleteAnnotationByIds(content.deleteOfflineList, decoded.userId);

            socket.emit("deleteAnnotationsConnectFeedback", {})

            for (let sharedUser of sharedUsers) {
                let socketClientId = socketManager.getSocketByUserId(sharedUser.userId);
                if (socketClientId)
                    nsp020.to(socketClientId).emit('sharedAnnotationDeleted', {annotation_Id: sharedUser.annotationId});
            }
        });


        socket.on("deleteAnnotations", async function (content: { token: string, ids: string[] }) {
            logger.debug('deleteAnnotations');
            let decoded = await checkToken(content, socket);
            if (!decoded)
                return;

            let sharedUsers = await annotationManager.deleteAnnotationByIds(content.ids, decoded.userId);

            socket.emit("deleteAnnotationFeedback", {})

            for (let sharedUser of sharedUsers) {
                let socketClientId = socketManager.getSocketByUserId(sharedUser.userId);
                if (socketClientId)
                    nsp020.to(socketClientId).emit('sharedAnnotationDeleted', {annotation_Id: sharedUser.annotationId});
            }
        });


        socket.on('disconnect', function () {
            logger.debug('disconnect');
        });


    });


    return nsp020;


}


