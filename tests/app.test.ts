import * as request from "supertest";
import app from '../src/app';
import {createConnection, getConnection} from "typeorm";
import loadFixturesHelper from "typeorm-fixtures-test";


beforeAll(async () => {
    await createConnection()
});

afterAll(async () => {
    getConnection().close().then(() => {
    });
})

beforeEach(async () => {
    try {
        await loadFixturesHelper();
    } catch (err) {
        console.log(err);
    }
});


describe('GET /version', function () {
    it('should respond version in json', function (done) {
        request(app)
            .get('/version')
            //.set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                expect(res.body).toMatchObject({version: '4.0.0'})
                done();
            });
    });
});



describe('GET /checkConnection', function () {
    it('should respond legacy version number in string', function (done) {
        request(app)
            .get('/checkConnection')
            .expect(200)
            .end(function (err, res) {
                expect(res.text).toBe('3.1.0');
                done();
            });
    });
});
