import {createConnection, getConnection, getRepository} from "typeorm";
import loadFixturesHelper from "typeorm-fixtures-test";
import * as request from "supertest";
import app from "../../src/app";
import {authenticationService} from "../../src/service/AuthenticationService";
import {jwtService} from "../../src/service/JwtService";
import * as fse from 'fs-extra';
import {Convocation} from "../../src/entity/Convocation";
import {User} from "../../src/entity/User";
import {AttendanceEnum} from "../../src/AttendanceEnum";


const libricielStructureId = '36603ba3-6001-4012-a251-819395dbd5ab';
const sittingBureauId = '9ccccf24-3d8e-4240-9bcf-bb962c1e1027';


beforeAll(async () => {
    await createConnection()
});

afterAll(async () => {
    getConnection().close().then(() => {
    });
})

beforeEach(async () => {
    fse.removeSync(`/tmp/${libricielStructureId}`);

    try {
        await loadFixturesHelper();
    } catch (err) {
        console.log(err);
    }
});


async function getToken(username: string, password: string): Promise<string> {
    let loggedInUser = await authenticationService.verifyPassword(username, password);
    return jwtService.sign(loggedInUser)
}


describe('POST /0.2.0/attendance/sittingId/:sittingId', function () {
    it('should return  200 and set status to absent ', async (done) => {

        const userRepository = getRepository(User);
        const convocationRepository = getRepository(Convocation);
        const username = 'actor1@libriciel.fr'
        const user = await userRepository.findOne({where: {username: username}});

        const token = await getToken(username, 'password');

        request(app)
            .post(`/0.2.0/attendance/sittings/${sittingBureauId}`)
            .set('Accept', 'application/json')
            .send({attendanceStatus : {attendance: 'absent'}, token: token})
            .end(async function (err, res) {
                expect(res.status).toBe(200);

                const convocation = await convocationRepository.findOne({where: {sittingId: sittingBureauId, userId: user.id}});

                expect(convocation.attendance).toBe(AttendanceEnum.Absent);

                done();
            });
    });


    it('should set status to absent_deputy and deputyId', async (done) => {

        const userRepository = getRepository(User);
        const convocationRepository = getRepository(Convocation);
        const username = 'thomas@libriciel.fr'
        const user = await userRepository.findOne({where: {username: username}});

        const token = await getToken(username, 'password');

        request(app)
            .post(`/0.2.0/attendance/sittings/${sittingBureauId}`)
            .set('Accept', 'application/json')
            .send({attendanceStatus : {attendance: 'deputy'}, token: token})
            .end(async function (err, res) {
                expect(res.status).toBe(200);

                const convocation = await convocationRepository.findOne({where: {sittingId: sittingBureauId, userId: user.id}});

                expect(convocation.attendance).toBe(AttendanceEnum.AbsentDeputy);
                expect(convocation.deputyId).toBeTruthy();

                done();
            });
    });



    it('should set status to absent cause no deputy', async (done) => {

        const userRepository = getRepository(User);
        const convocationRepository = getRepository(Convocation);
        const username = 'actor1@libriciel.fr'
        const user = await userRepository.findOne({where: {username: username}});

        const token = await getToken(username, 'password');

        request(app)
            .post(`/0.2.0/attendance/sittings/${sittingBureauId}`)
            .set('Accept', 'application/json')
            .send({attendanceStatus : {attendance: 'deputy'}, token: token})
            .end(async function (err, res) {
                expect(res.status).toBe(200);

                const convocation = await convocationRepository.findOne({where: {sittingId: sittingBureauId, userId: user.id}});

                expect(convocation.attendance).toBe(AttendanceEnum.Absent);
                expect(convocation.deputyId).toBeFalsy();

                done();
            });
    });


    it('should set status to poa and mandatorId should be not null', async (done) => {

        const userRepository = getRepository(User);
        const convocationRepository = getRepository(Convocation);
        const username = 'actor1@libriciel.fr'
        const user = await userRepository.findOne({where: {username: username}});

        const mandator = await userRepository.findOne({where: {username: 'actor2@libriciel.fr'}});

        const token = await getToken(username, 'password');

        request(app)
            .post(`/0.2.0/attendance/sittings/${sittingBureauId}`)
            .set('Accept', 'application/json')
            .send({attendanceStatus : {attendance: 'poa', mandatorId: mandator.id}, token: token})
            .end(async function (err, res) {
                expect(res.status).toBe(200);

                const convocation = await convocationRepository.findOne({where: {sittingId: sittingBureauId, userId: user.id}});

                expect(convocation.attendance).toBe(AttendanceEnum.AbsentMandator);
                expect(convocation.deputyId).toBeFalsy();
                expect(convocation.mandatorId).toBeTruthy();

                done();
            });
    });


    it('should set status to present and set mandatorId to null', async (done) => {

        const userRepository = getRepository(User);
        const convocationRepository = getRepository(Convocation);
        const username = 'arnaud_owner@libriciel.fr'
        const user = await userRepository.findOne({where: {username: username}});

        const token = await getToken(username, 'password');

        request(app)
            .post(`/0.2.0/attendance/sittings/${sittingBureauId}`)
            .set('Accept', 'application/json')
            .send({attendanceStatus : {attendance: 'present', mandatorId: null}, token: token})
            .end(async function (err, res) {
                expect(res.status).toBe(200);

                const convocation = await convocationRepository.findOne({where: {sittingId: sittingBureauId, userId: user.id}});

                expect(convocation.attendance).toBe(AttendanceEnum.Present);
                expect(convocation.deputyId).toBeFalsy();
                expect(convocation.mandatorId).toBeFalsy();

                done();
            });
    });



});



describe('GET /0.2.0/attendance/sittingId/:sittingId', function () {
    it('should return attendanceStatus and available mandators. hasDeputy should be false ', async (done) => {

        const userRepository = getRepository(User);
        const username = 'actor1@libriciel.fr'
        const user = await userRepository.findOne({where: {username: username}});
        const token = await getToken(username, 'password');

        request(app)
            .get(`/0.2.0/attendance/sittings/${sittingBureauId}`)
            .set('Accept', 'application/json')
            .set('token', token)
            .end(async function (err, res) {
                expect(res.status).toBe(200);

                expect(res.body.attendance).toBe('present');
                expect(res.body.hasDeputy).toBe(false);
                expect(res.body.mandatorId).toBe(null);
                expect(res.body.availableMandators).toMatchObject(expect.any(Array));
                done();
            });
    });

    it('should return attendanceStatus deputy and hasDeputy to true ', async (done) => {

        const userRepository = getRepository(User);
        const username = 'thomas@libriciel.fr'
        const user = await userRepository.findOne({where: {username: username}});
        const token = await getToken(username, 'password');

        request(app)
            .get(`/0.2.0/attendance/sittings/${sittingBureauId}`)
            .set('Accept', 'application/json')
            .set('token', token)
            .end(async function (err, res) {
                expect(res.status).toBe(200);

                expect(res.body.attendance).toBe('deputy');
                expect(res.body.hasDeputy).toBe(true);
                expect(res.body.mandatorId).toBe(null);
                expect(res.body.availableMandators).toMatchObject(expect.any(Array));
                done();
            });
    });


    it('should return attendanceStatus poa and has no Deputy and has a mandatorId ', async (done) => {

        const userRepository = getRepository(User);
        const username = 'arnaud_owner@libriciel.fr'
        const user = await userRepository.findOne({where: {username: username}});
        const token = await getToken(username, 'password');

        request(app)
            .get(`/0.2.0/attendance/sittings/${sittingBureauId}`)
            .set('Accept', 'application/json')
            .set('token', token)
            .end(async function (err, res) {
                expect(res.status).toBe(200);

                expect(res.body.attendance).toBe('poa');
                expect(res.body.hasDeputy).toBe(false);
                expect(res.body.mandatorId).toBeTruthy();
                expect(res.body.availableMandators).toMatchObject(expect.any(Array));
                done();
            });
    });

});
