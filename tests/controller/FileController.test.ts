import {createConnection, getConnection} from "typeorm";
import loadFixturesHelper from "typeorm-fixtures-test";
import * as request from "supertest";
import app from "../../src/app";
import {authenticationService} from "../../src/service/AuthenticationService";
import {jwtService} from "../../src/service/JwtService";
import * as fse from 'fs-extra';
import * as path from "path";


beforeAll(async () => {
    await createConnection()
});

afterAll(async () => {
    getConnection().close().then(() => {
    });
})

beforeEach(async () => {
    fse.removeSync('/tmp/simpleFile');

    try {
        await loadFixturesHelper();
    } catch (err) {
        console.log(err);
    }
});


async function getToken(username: string, password: string): Promise<string> {
    let loggedInUser = await authenticationService.verifyPassword(username, password);
    return jwtService.sign(loggedInUser)
}


describe('/0.2.0/projets/dlPdf/:id', function () {
    it('should return a 403 cause no token in header', async (done) => {
        request(app)
            .get(`/0.2.0/projets/dlPdf/projectId`)
            .set('Accept', 'application/json')

            .end(function (err, res) {
                expect(res.status).toBe(403);
                done();
            });
    });


    it('should return a 403 cause bad token in header', async (done) => {
        request(app)
            .get(`/0.2.0/projets/dlPdf/projectId`)
            .set('Accept', 'application/json')
            .set('token', '1234')
            .end(function (err, res) {
                expect(res.status).toBe(403);
                done();
            });
    });

    it('should return a 403 cause no token in body', async (done) => {
        request(app)
            .post(`/0.2.0/projets/dlPdf/projectId`)
            .set('Accept', 'application/json')
            .end(function (err, res) {
                expect(res.status).toBe(403);
                done();
            });
    });


    it('should return a 403 cause no token in body', async (done) => {
        request(app)
            .post(`/0.2.0/projets/dlPdf/projectId`)
            .set('Accept', 'application/json')
            .end(function (err, res) {
                expect(res.status).toBe(403);
                done();
            });
    });





    it('should return a 404 cause fileid does not exist', async (done) => {
        //const simpleFileId = 'd3c820ef-97de-42d8-a4f2-5c1cfd5dbbbe'
        const token = await getToken('actor1@libriciel.fr', 'password')
        const notExistFileId = 'e4c820ef-97de-42d8-a4f2-5c1cfd5dbbbe';
        request(app)
            .get(`/0.2.0/projets/dlPdf/${notExistFileId}`)
            .set('Accept', 'application/json')
            .set('token', token)
            .end(function (err, res) {
                expect(res.status).toBe(404);
                done();
            });

    });

    it('should return a 404 cause fileId exists but file does not exist', async (done) => {
        const simpleFileId = 'd3c820ef-97de-42d8-a4f2-5c1cfd5dbbbe'

        const token = await getToken('actor1@libriciel.fr', 'password')
        request(app)
            .get(`/0.2.0/projets/dlPdf/${simpleFileId}`)
            .set('Accept', 'application/json')
            .set('token', token)
            .end(function (err, res) {
                expect(res.status).toBe(404);
                done();
            });


    });


    it('should return  200 and file ', async (done) => {
        const simpleFileId = 'd3c820ef-97de-42d8-a4f2-5c1cfd5dbbbe'
        await fse.copyFile(path.resolve(__dirname + '../../assets/simpleFile.pdf'), '/tmp/simpleFile');

        const token = await getToken('actor1@libriciel.fr', 'password')
        request(app)
            .get(`/0.2.0/projets/dlPdf/${simpleFileId}`)
            .set('Accept', 'application/json')
            .set('token', token)
            .end(function (err, res) {
                expect(res.status).toBe(200);
                expect(res.headers['content-type']).toBe('application/pdf');
                expect(res.headers['content-length']).toBe('21742');
                done();
            });


    });



    describe('/0.2.0/projets/dlInvitation/:id', function () {
        it('should return a 200 and the file', async (done) => {
            await fse.copyFile(path.resolve(__dirname + '../../assets/simpleFile.pdf'), '/tmp/simpleFile');
            const invitationId = '3480b1fd-3346-4c35-952c-4756e69b06d2';
            const token = await getToken('employee@libriciel.fr', 'password')

            request(app)
                .get(`/0.2.0/projets/dlInvitation/${invitationId}`)
                .set('Accept', 'application/json')
                .set('token', token)
                .end(function (err, res) {
                    expect(res.status).toBe(200);
                    expect(res.headers['content-type']).toBe('application/pdf');
                    expect(res.headers['content-length']).toBe('21742');
                    done();
                });
        });
    });


    describe('/0.2.0/annexes/dlAnnexe/:id', function () {
        it('should return a 200 and the file', async (done) => {
            await fse.copyFile(path.resolve(__dirname + '../../assets/simpleFile.pdf'), '/tmp/simpleFile');
            const annexId = 'cb712bf9-5370-4c23-9048-8895a3286fa5';
            const token = await getToken('actor1@libriciel.fr', 'password')

            request(app)
                .get(`/0.2.0/annexes/dlAnnexe/${annexId}`)
                .set('Accept', 'application/json')
                .set('token', token)
                .end(function (err, res) {
                    expect(res.status).toBe(200);
                    expect(res.headers['content-type']).toBe('application/pdf');
                    expect(res.headers['content-length']).toBe('21742');
                    done();
                });
        });
    });



});
