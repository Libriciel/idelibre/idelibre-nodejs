import * as request from "supertest";
import {createConnection, getConnection, getRepository} from "typeorm";
import loadFixturesHelper from "typeorm-fixtures-test";
import app from "../../src/app";
import {User} from "../../src/entity/User";
import {io, Socket} from "socket.io-client";
import * as http from "http";
import {Server} from "socket.io";
import {initSocketIo} from "../../src/socketIo/0.2.0/initSocketIo";
import {socketManager} from "../../src/service/SocketManager";
import {nsp20} from "../../src/socketIo/0.2.0/SocketNamespace";
import config from "../../config";


let socket: Socket;
let httpServer;
let httpServerHost = '127.0.0.1';
let httpServerPort = 1234;
let socketNamespace = '0.2.0'
let ioServer;


beforeAll(async (done) => {
    await createConnection();
    httpServer = http.createServer().listen(httpServerPort, httpServerHost)
    ioServer = new Server(httpServer, {cors: {origin: "*"}});
    const namespace = initSocketIo(ioServer);
    nsp20.setNamespace(namespace);
    done();
});

afterAll(async (done) => {
    ioServer.close();
    httpServer.close();
    await getConnection().close()
    done();
});


beforeEach(async (done) => {
    try {
        await loadFixturesHelper();
    } catch (err) {
        console.log(err);
    }

    socket = io(`http://${httpServerHost}:${httpServerPort}/${socketNamespace}`);
    socket.on('connect', () => {
        done();
    });

});

afterEach((done) => {
    socket.disconnect();
    socketManager.clear();
    done();
});


describe('POST /notifications/sittings/new', function () {
    it('should emit newSeance', async (done) => {
        const userRepository = getRepository(User);
        const actor = await userRepository.findOne({username: 'actor1@libriciel.fr'});
        socketManager.associateClientSocket(actor.id, socket.id);

        socket.on('newSeance', (data) => {
            expect(data).toMatchObject({message: "newSeance"});
            done()
        })

        request(app)
            .post(`/notifications/sittings/new`)
            .send({passphrase: config.passphrase, userIds: [actor.id]})
            .end(function (err, res) {
                expect(res.status).toBe(204);
            });
    });

    it('should send 403 wrong passphrase', async (done) => {
        const userRepository = getRepository(User);
        const actor = await userRepository.findOne({username: 'actor1@libriciel.fr'});

        request(app)
            .post(`/notifications/sittings/new`)
            .send({passphrase: "fake", userIds: [actor.id]})
            .end(function (err, res) {
                expect(res.status).toBe(403);
                done();
            });
    });


    it('should emit nothing, not associated userId', async (done) => {
        const userRepository = getRepository(User);
        const actor = await userRepository.findOne({username: 'actor1@libriciel.fr'});

        request(app)
            .post(`/notifications/sittings/new`)
            .set('Accept', 'application/json')
            .send({passphrase: config.passphrase, userIds: [actor.id]})
            .expect(200)
            .end(function (err, res) {
                expect(res.status).toBe(204);
                done();
            });
    });


    it('should emit newSeance only to this specific user', async (done) => {
        const userRepository = getRepository(User);
        const actor = await userRepository.findOne({username: 'actor1@libriciel.fr'});
        socketManager.associateClientSocket(actor.id, socket.id);

        let socketOtherUser = io(`http://${httpServerHost}:${httpServerPort}/${socketNamespace}`);
        socketOtherUser.on("connect", () => {
            request(app)
                .post(`/notifications/sittings/new`)
                .set('Accept', 'application/json')
                .send({passphrase: config.passphrase, userIds: [actor.id]})
                .expect(204)
                .end(function (err, res) {
                    expect(res.status).toBe(204);
                });
        })

        socketOtherUser.on('newSeance', (data) => {
            socketOtherUser.disconnect();
            fail('only the selected user should receive the notification')
        });

        setTimeout(() => {
            socketOtherUser.disconnect();
            done()
        }, 200);

    });


});


describe('POST /notifications/sittings/modify', function () {
    it('should emit modifiedSeance', async (done) => {
        const userRepository = getRepository(User);
        const actor = await userRepository.findOne({username: 'actor1@libriciel.fr'});
        socketManager.associateClientSocket(actor.id, socket.id);

        socket.on('modifiedSeance', (data) => {
            expect(data).toMatchObject({message: "modifiedSeance"});
            done()
        })

        request(app)
            .post(`/notifications/sittings/modify`)
            .set('Accept', 'application/json')
            .send({passphrase: config.passphrase, userIds: [actor.id]})
            .expect(200)
            .end(function (err, res) {
                expect(res.status).toBe(204);
            });
    });
});


describe('POST /notifications/sittings/remove', function () {
    it('should emit removeSeance', async (done) => {
        const userRepository = getRepository(User);
        const actor = await userRepository.findOne({username: 'actor1@libriciel.fr'});
        socketManager.associateClientSocket(actor.id, socket.id);

        socket.on('removeSeance', (data) => {
            expect(data).toMatchObject({message: "removeSeance"});
            done()
        })

        request(app)
            .post(`/notifications/sittings/remove`)
            .set('Accept', 'application/json')
            .send({passphrase: config.passphrase, userIds: [actor.id]})
            .expect(200)
            .end(function (err, res) {
                expect(res.status).toBe(204)
            });
    });
});







