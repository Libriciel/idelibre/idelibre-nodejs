import {createConnection, getConnection} from "typeorm";
import loadFixturesHelper from "typeorm-fixtures-test";
import * as request from "supertest";
import app from "../../src/app";
import {authenticationService} from "../../src/service/AuthenticationService";
import {jwtService} from "../../src/service/JwtService";
import Config from "../../config";
import mockAxios from 'axios';


beforeAll(async () => {
    await createConnection()
});

afterAll(async () => {
    getConnection().close().then(() => {
    });
})

beforeEach(async () => {
    try {
        await loadFixturesHelper();
    } catch (err) {
        console.log(err);
    }
});


async function getToken(username: string, password: string): Promise<string> {
    let loggedInUser = await authenticationService.verifyPassword(username, password);
    return jwtService.sign(loggedInUser)
}


describe('/0.2.0/users/changePassword', function () {
    it('should return  200', async (done) => {
        const token = await getToken('actor1@libriciel.fr', 'password');


        mockAxios.post = jest.fn().mockResolvedValueOnce({status: 200, response: {data: {message: 'success'}}});

        request(app)
            .post(`/0.2.0/users/changePassword`)
            .send({currentPassword: 'password', newPassword: 'newPassword', token: token})
            .end(async function (err, res) {
                expect(res.status).toBe(200)
                expect(res.body.message).toBe('passwordChanged');
                done();
            });
    });


    it('should return  400 bad password', async (done) => {
        const token = await getToken('actor1@libriciel.fr', 'password');

        mockAxios.post = jest.fn().mockRejectedValueOnce({
            status: 400,
            response: {
                data: {message: 'INVALID_CURRENT_PASSWORD'}
            }
        });

        request(app)
            .post(`/0.2.0/users/changePassword`)
            .send({currentPassword: 'falsePassword', newPassword: 'newPassword', token: token})
            .end(async function (err, res) {
                expect(res.status).toBe(400)
                expect(res.body.message).toBe('La saisie de votre mot de passe actuel est incorrecte. ');
                done();
            });
    });


    it('should return  400 entropyToLow', async (done) => {
        const token = await getToken('actor1@libriciel.fr', 'password');

        mockAxios.post = jest.fn().mockRejectedValueOnce({
            status: 400,
            response: {
                data: {message: 'ENTROPY_TOO_LOW', minEntropyValue: 80, currentEntropyValue: 50}
            }
        });

        request(app)
            .post(`/0.2.0/users/changePassword`)
            .send({currentPassword: 'password', newPassword: 'tooLow', token: token})
            .end(async function (err, res) {
                expect(res.status).toBe(400)
                expect(res.body.message).toBe('Le mot de passe n\'est pas assez fort. ');
                expect(res.body.minEntropyValue).toBe(80);
                expect(res.body.currentEntropyValue).toBe(50);
                done();
            });
    });

    it('should return  400 unknown error', async (done) => {
        const token = await getToken('actor1@libriciel.fr', 'password');

        mockAxios.post = jest.fn().mockRejectedValueOnce({
            status: 400,
            data: {message: 'UNHANDLED_ERROR'}
        });

        request(app)
            .post(`/0.2.0/users/changePassword`)
            .send({currentPassword: 'password', newPassword: 'newPassword', token: token})
            .end(async function (err, res) {
                expect(res.status).toBe(400)
                expect(res.body.message).toBe('Une erreur est survenue');
                expect(res.body.minEntropyValue).toBeFalsy();
                expect(res.body.currentEntropyValue).toBeFalsy();
                done();
            });
    });


});
