import {createConnection, getConnection} from "typeorm";
import loadFixturesHelper from "typeorm-fixtures-test";
import * as request from "supertest";
import app from "../../src/app";
import {authenticationService} from "../../src/service/AuthenticationService";
import {jwtService} from "../../src/service/JwtService";
import * as fse from 'fs-extra';
import * as path from "path";
import config from "../../config";


const libricielStructureId = '36603ba3-6001-4012-a251-819395dbd5ab';
const sittingBureauId = '9ccccf24-3d8e-4240-9bcf-bb962c1e1027';
const zipFilePath = config.zipDirectory + `${libricielStructureId}/${sittingBureauId}.zip`


beforeAll(async () => {
    await createConnection()
});

afterAll(async () => {
    getConnection().close().then(() => {
    });
})

beforeEach(async () => {
    fse.removeSync(`/tmp/${libricielStructureId}`);

    try {
        await loadFixturesHelper();
    } catch (err) {
        console.log(err);
    }
});


async function getToken(username: string, password: string): Promise<string> {
    let loggedInUser = await authenticationService.verifyPassword(username, password);
    return jwtService.sign(loggedInUser)
}


describe('/0.2.0/zips/dlZip/:sittingId', function () {
    it('should return  200 and file ', async (done) => {
        await fse.mkdirp(config.zipDirectory + `${libricielStructureId}`);
        await fse.copyFile(path.resolve(__dirname + '../../assets/simpleFile.zip'), zipFilePath);

        const token = await getToken('actor1@libriciel.fr', 'password');
        request(app)
            .get(`/0.2.0/zips/dlZip/${sittingBureauId}`)
            .set('Accept', 'application/json')
            .set('token', token)
            .end(function (err, res) {
                expect(res.status).toBe(200);
                expect(res.headers['content-type']).toBe('application/zip');
                expect(res.headers['content-length']).toBe('14537');
                done();
            });
    });


});
