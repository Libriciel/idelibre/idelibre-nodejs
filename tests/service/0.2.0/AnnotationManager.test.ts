import {createConnection, getConnection, getManager, getRepository} from "typeorm";
import loadFixturesHelper from "typeorm-fixtures-test";
import {User} from "../../../src/entity/User";
import {annotationManager} from "../../../src/service/0.2.0/AnnotationManager";
import {AnnotationUser} from "../../../src/entity/AnnotationUser";
import {AnnotationFromClient} from "../../../src/service/0.2.0/Annotation/AnnotationFromClient";
import {Annotation} from "../../../src/entity/Annotation";
import any = jasmine.any;
import {Configuration} from "../../../src/entity/Configuration";

beforeAll(async () => {
    await createConnection()
});

afterAll(async () => {
    getConnection().close().then(() => {
    });
})

beforeEach(async () => {
    try {
        await loadFixturesHelper();
    } catch (err) {
        console.log(err);
    }
});


describe('getAnnotations', function () {
    it('it should private and shared annotations (author + recipient', async function () {
        const userRepository = getRepository(User);
        const user = await userRepository.findOne({username: 'actor1@libriciel.fr'});
        const annotations = await annotationManager.getAnnotations(user.id, user.structureId);
        expect(annotations).toHaveLength(3);

        const sharedByActor1Annotation = annotations.filter(annot => annot.annotation_id === '4d4c6fcd-2f48-4e2d-b9ef-bdf408d03faa');
        expect(sharedByActor1Annotation).toHaveLength(1);
        expect(sharedByActor1Annotation[0]).toMatchObject({
                annotation_id: '4d4c6fcd-2f48-4e2d-b9ef-bdf408d03faa',
                annotation_author_id: any(String),
                annotation_author_name: 'Libriciel',
                annotation_annexe_id: null,
                annotation_projet_id: any(String),
                annotation_seance_id: null,
                annotation_page: 1,
                annotation_rect: {top: 1, left: 1, bottom: 3, right: 2},
                annotation_text: 'mon annotation partagée (par actor1)',
                annotation_date: 1612962000000,
                annotation_shareduseridlist: [
                    any(String),
                    any(String)
                ],
                isread: true
            }
        )
    });
    it('it should return private annotations only (isSharedAnnotation = false)', async function () {
        const userRepository = getRepository(User);
        const user = await userRepository.findOne({username: 'actor1@libriciel.fr'});

        const configuration = await getRepository(Configuration).findOne({structureId: user.structureId});
        configuration.isSharedAnnotation = false;
        await getManager().save(configuration);

        const annotations = await annotationManager.getAnnotations(user.id, user.structureId);
        expect(annotations).toHaveLength(2);
    });
});


describe('setAnnotation to read', function () {
    it('it should set annotations to read', async function () {
        const user = await getRepository(User).findOne({username: 'actor1@libriciel.fr'});
        const annotationUserRepo = getRepository(AnnotationUser);
        const annotationsUsers = await annotationUserRepo.find({userId: user.id});
        const annotationIds = annotationsUsers.map((el) => el.annotationId);
        const isSuccess = await annotationManager.setAnnotationToRead(user.id, annotationIds)

        expect(isSuccess).toBe(true)
        const updatedAnnotationsUser = await annotationUserRepo.findOne({userId: user.id, annotationId: annotationIds[0]})
        expect(updatedAnnotationsUser.isRead).toBe(true)
    });
});


describe('setAnnotation to read', function () {
    it('it should set annotations to read', async function () {
        const user = await getRepository(User).findOne({username: 'actor1@libriciel.fr'});
        const annotationUserRepo = getRepository(AnnotationUser);
        const annotationsUsers = await annotationUserRepo.find({userId: user.id});
        const annotationIds = annotationsUsers.map((el) => el.annotationId);
        const isSuccess = await annotationManager.setAnnotationToRead(user.id, annotationIds)

        expect(isSuccess).toBe(true)
        const updatedAnnotationsUser = await annotationUserRepo.findOne({userId: user.id, annotationId: annotationIds[0]})
        expect(updatedAnnotationsUser.isRead).toBe(true)
    });

});


describe('addOrUpdateAnnotation', function () {
    it('it Add a new Annotation with sharedUsers', async function () {
        const actor = await getRepository(User).findOne({username: 'actor1@libriciel.fr'});
        const actor2 = await getRepository(User).findOne({username: 'actor2@libriciel.fr'});
        const actor3 = await getRepository(User).findOne({username: 'actor3@libriciel.fr'});
        const annex1Id = "cb712bf9-5370-4c23-9048-8895a3286fa5"


        let annotationClient = new AnnotationFromClient();
        annotationClient.annotation_id = 'eb712bf9-5370-4c23-9048-8895a3286fa3'
        annotationClient.annotation_author_id = actor.id
        annotationClient.annotation_page = 1
        annotationClient.originType = "Annexe"
        annotationClient.originId = annex1Id
        annotationClient.annotation_shareduseridlist = [actor2.id, actor3.id]
        annotationClient.annotation_text = "New annotation shared annotations"
        annotationClient.annotation_date = (new Date()).getTime();
        annotationClient.annotation_author_name = 'not real name'
        annotationClient.annotation_rect = {top: 1, left: 1, bottom: 1, right: 1}

        let annotationJson = JSON.stringify(annotationClient)

        const toNotify = await annotationManager.addOrUpdateAnnotations([annotationJson], actor.id);

        expect(toNotify).toMatchObject({
            deletedSharedUser: [],
            addedSharedUser: [
                {
                    userId: any(String),
                    annotation: any(Object)
                },
                {
                    userId: any(String),
                    annotation: any(Object)
                }
            ]
        });

        const addedAnnotation = await getRepository(Annotation).find({
            where: {id: 'eb712bf9-5370-4c23-9048-8895a3286fa3'},
            relations: ['annotationUsers']
        },);
        expect(addedAnnotation).toHaveLength(1);
        expect(addedAnnotation[0]).toMatchObject({
            id: 'eb712bf9-5370-4c23-9048-8895a3286fa3',
            authorId: actor.id,
            annexId: annex1Id,
            projectId: null,
            sittingId: null,
            page: 1,
            rect: {top: 1, left: 1, right: 1, bottom: 1},
            text: 'New annotation shared annotations',
            createdAt: any(Date),
            annotationUsers: [any(AnnotationUser), any(AnnotationUser)]
        })

    });

    it('it update an annotation who has sharedUsers with  no sharedUsers', async function () {
        const actor = await getRepository(User).findOne({username: 'actor1@libriciel.fr'});
        const actor2 = await getRepository(User).findOne({username: 'actor2@libriciel.fr'});
        const actor3 = await getRepository(User).findOne({username: 'actor3@libriciel.fr'});

        const project2Id = "bd67266e-c858-41e3-98b6-087ebd5835c3"
        const annotationId = "4d4c6fcd-2f48-4e2d-b9ef-bdf408d03faa"


        let annotationClient = new AnnotationFromClient();
        annotationClient.annotation_id = annotationId
        annotationClient.annotation_author_id = actor.id
        annotationClient.annotation_page = 1
        annotationClient.originType = "Projet"
        annotationClient.originId = project2Id
        annotationClient.annotation_text = "Modified annotation"
        annotationClient.annotation_date = (new Date()).getTime();
        annotationClient.annotation_author_name = 'actor1'
        annotationClient.annotation_rect = {top: 1, left: 1, bottom: 1, right: 1}

        let annotationJson = JSON.stringify(annotationClient)

        const toNotify = await annotationManager.addOrUpdateAnnotations([annotationJson], actor.id);

        expect(toNotify).toMatchObject({
            deletedSharedUser: [
                {
                    userId: actor2.id,
                    annotationId: annotationId
                },
                {
                    userId: actor3.id,
                    annotationId: annotationId
                }
            ],
            addedSharedUser: []
        });

        const addedAnnotation = await getRepository(Annotation).find({
            where: {id: annotationId},
            relations: ['annotationUsers']
        },);
        expect(addedAnnotation).toHaveLength(1);
        expect(addedAnnotation[0]).toMatchObject({
            id: annotationId,
            authorId: actor.id,
            annexId: null,
            projectId: project2Id,
            sittingId: null,
            page: 1,
            rect: {top: 1, left: 1, right: 1, bottom: 1},
            text: 'Modified annotation',
            createdAt: any(Date),
            annotationUsers: []
        })

    });


    it('it update an annotation with sharedUsers', async function () {
        const actor = await getRepository(User).findOne({username: 'actor1@libriciel.fr'});
        const actor2 = await getRepository(User).findOne({username: 'actor2@libriciel.fr'});
        const actor3 = await getRepository(User).findOne({username: 'actor3@libriciel.fr'});
        const actor4 = await getRepository(User).findOne({username: 'actor4@libriciel.fr'});
        const project2Id = "bd67266e-c858-41e3-98b6-087ebd5835c3"
        const annotationId = "4d4c6fcd-2f48-4e2d-b9ef-bdf408d03faa"


        let annotationClient = new AnnotationFromClient();
        annotationClient.annotation_id = annotationId
        annotationClient.annotation_author_id = actor.id
        annotationClient.annotation_page = 1
        annotationClient.originType = "Projet"
        annotationClient.originId = project2Id
        annotationClient.annotation_shareduseridlist = [actor2.id, actor4.id]
        annotationClient.annotation_text = "Modified annotation"
        annotationClient.annotation_date = (new Date()).getTime();
        annotationClient.annotation_author_name = 'actor1'
        annotationClient.annotation_rect = {top: 1, left: 1, bottom: 1, right: 1}

        let annotationJson = JSON.stringify(annotationClient)

        const toNotify = await annotationManager.addOrUpdateAnnotations([annotationJson], actor.id);

        expect(toNotify).toMatchObject({
            deletedSharedUser: [
                {
                    userId: actor2.id,
                    annotationId: annotationId
                },
                {
                    userId: actor3.id,
                    annotationId: annotationId
                }
            ],
            addedSharedUser: [
                {
                    userId: actor2.id,
                    annotation: any(Object)
                },
                {
                    userId: actor4.id,
                    annotation: any(Object)
                }
            ]
        });

        const addedAnnotation = await getRepository(Annotation).find({
            where: {id: annotationId},
            relations: ['annotationUsers']
        },);
        expect(addedAnnotation).toHaveLength(1);
        expect(addedAnnotation[0]).toMatchObject({
            id: annotationId,
            authorId: actor.id,
            annexId: null,
            projectId: project2Id,
            sittingId: null,
            page: 1,
            rect: {top: 1, left: 1, right: 1, bottom: 1},
            text: 'Modified annotation',
            createdAt: any(Date),
            annotationUsers: [any(AnnotationUser), any(AnnotationUser)]
        })

        expect(addedAnnotation[0].annotationUsers).toHaveLength(2);
        // Quick and dirty test ! create custom matcher or !
        expect(addedAnnotation[0].annotationUsers[0].userId === actor2.id ||  addedAnnotation[0].annotationUsers[1].userId === actor2.id).toBeTruthy();
        expect(addedAnnotation[0].annotationUsers[0].userId === actor4.id ||  addedAnnotation[0].annotationUsers[1].userId === actor4.id).toBeTruthy();
    });


    it('it update an  private annotation with sharedUsers', async function () {
        const actor = await getRepository(User).findOne({username: 'actor1@libriciel.fr'});
        const actor2 = await getRepository(User).findOne({username: 'actor2@libriciel.fr'});
        const actor3 = await getRepository(User).findOne({username: 'actor3@libriciel.fr'});
        const actor4 = await getRepository(User).findOne({username: 'actor4@libriciel.fr'});
        const project3Id = "b87d63e2-4d86-43b9-b16d-a7ccef4568bf"
        const privateAnnotationId = "1a0db141-f414-4393-ab61-45512bd6c9bc"


        let annotationClient = new AnnotationFromClient();
        annotationClient.annotation_id = privateAnnotationId
        annotationClient.annotation_author_id = actor.id
        annotationClient.annotation_page = 1
        annotationClient.originType = "Projet"
        annotationClient.originId = project3Id
        annotationClient.annotation_shareduseridlist = [actor2.id, actor4.id]
        annotationClient.annotation_text = "Modified annotation"
        annotationClient.annotation_date = (new Date()).getTime();
        annotationClient.annotation_author_name = 'actor1'
        annotationClient.annotation_rect = {top: 1, left: 1, bottom: 1, right: 1}

        let annotationJson = JSON.stringify(annotationClient)

        const toNotify = await annotationManager.addOrUpdateAnnotations([annotationJson], actor.id);

        expect(toNotify).toMatchObject({
            deletedSharedUser: [],
            addedSharedUser: [
                {
                    userId: actor2.id,
                    annotation: any(Object)
                },
                {
                    userId: actor4.id,
                    annotation: any(Object)
                }
            ]
        });

        const addedAnnotation = await getRepository(Annotation).find({
            where: {id: privateAnnotationId},
            relations: ['annotationUsers']
        },);
        expect(addedAnnotation).toHaveLength(1);
        expect(addedAnnotation[0]).toMatchObject({
            id: privateAnnotationId,
            authorId: actor.id,
            annexId: null,
            projectId: project3Id,
            sittingId: null,
            page: 1,
            rect: {top: 1, left: 1, right: 1, bottom: 1},
            text: 'Modified annotation',
            createdAt: any(Date),
            annotationUsers: [any(AnnotationUser), any(AnnotationUser)]
        })

        expect(addedAnnotation[0].annotationUsers).toHaveLength(2);
        // Quick and dirty test ! create custom matcher or !
        expect(addedAnnotation[0].annotationUsers[0].userId === actor2.id ||  addedAnnotation[0].annotationUsers[1].userId === actor2.id).toBeTruthy();
        expect(addedAnnotation[0].annotationUsers[0].userId === actor4.id ||  addedAnnotation[0].annotationUsers[1].userId === actor4.id).toBeTruthy();
    });


});
