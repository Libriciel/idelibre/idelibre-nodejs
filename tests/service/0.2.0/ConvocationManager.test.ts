import {createConnection, getConnection, getRepository, In} from "typeorm";
import loadFixturesHelper from "typeorm-fixtures-test";
import {convocationManager} from "../../../src/service/0.2.0/ConvocationManager";
import {User} from "../../../src/entity/User";
import {Convocation} from "../../../src/entity/Convocation";
import any = jasmine.any;
import * as fse from "fs-extra";
import config from "../../../config";
import {AttendanceEnum} from "../../../src/AttendanceEnum";


beforeAll(async () => {
    await createConnection()
});

afterAll(async () => {
    getConnection().close().then(() => {
    });
})

beforeEach(async () => {
    try {
        await loadFixturesHelper();
    } catch (err) {
        console.log(err);
    }
});


describe('update attendance status', function () {
    it('it should return true', async function () {
        const userRepository = getRepository(User);

        const actor = await userRepository.findOne({username: 'actor1@libriciel.fr'});
        const sittingBureauId = '9ccccf24-3d8e-4240-9bcf-bb962c1e1027';

        const isSuccess = await convocationManager.setAttendanceStatus(actor.id, sittingBureauId, AttendanceEnum.Present, 'Boris')
        expect(isSuccess).toBe(true);

        const convocationRepository = getRepository(Convocation);
        const convocation = await convocationRepository.findOne({userId: actor.id, sittingId: sittingBureauId});

        expect(convocation.attendance).toBe(AttendanceEnum.Present);
        expect(convocation.deputyId).toBe(null);

    })


    it('it should set a deputy', async function () {
        const userRepository = getRepository(User);

        const actor = await userRepository.findOne({username: 'thomas@libriciel.fr'});
        const sittingBureauId = '9ccccf24-3d8e-4240-9bcf-bb962c1e1027';


        const isSuccess = await convocationManager.setAttendanceStatus(actor.id, sittingBureauId, AttendanceEnum.AbsentDeputy, null)
        expect(isSuccess).toBe(true);

        const convocationRepository = getRepository(Convocation);
        const convocation = await convocationRepository.findOne({userId: actor.id, sittingId: sittingBureauId});

        expect(convocation.attendance).toBe(AttendanceEnum.AbsentDeputy);
        const uuidRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-[4][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
        expect(convocation.deputyId).toMatch(uuidRegex);

        const deputy = await userRepository.findOne(convocation.deputyId);
        expect(deputy.firstName).toBe('thomas_deputy');

    })



    it('it should set a mandator', async function () {
        const userRepository = getRepository(User);

        const actor = await userRepository.findOne({username: 'thomas@libriciel.fr'});
        const sittingBureauId = '9ccccf24-3d8e-4240-9bcf-bb962c1e1027';

        const mandator = await userRepository.findOne({username: 'actor1@libriciel.fr'});


        const isSuccess = await convocationManager.setAttendanceStatus(actor.id, sittingBureauId, AttendanceEnum.AbsentMandator, 'null', mandator.id)
        expect(isSuccess).toBe(true);

        const convocationRepository = getRepository(Convocation);
        const convocation = await convocationRepository.findOne({userId: actor.id, sittingId: sittingBureauId});

        expect(convocation.attendance).toBe(AttendanceEnum.AbsentMandator);
        const uuidRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-[4][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
        expect(convocation.mandatorId).toMatch(uuidRegex);

        const currentMandator = await userRepository.findOne(convocation.mandatorId);
        expect(currentMandator.username).toBe('actor1@libriciel.fr');
    })


    it('it should return false cause wrong status', async function () {
        const userRepository = getRepository(User);

        const actor = await userRepository.findOne({username: 'actor1@libriciel.fr'});
        const sittingBureauId = '9ccccf24-3d8e-4240-9bcf-bb962c1e1027';

        const isSuccess = await convocationManager.setAttendanceStatus(actor.id, sittingBureauId, 'notExist', 'Boris')
        expect(isSuccess).toBe(false);
    })


    it('it should return false cause wrong sittingId', async function () {
        const userRepository = getRepository(User);

        const actor = await userRepository.findOne({username: 'actor1@libriciel.fr'});
        const sittingIdNotExist = '9ccccf24-3d8e-4240-9bcf-bb962c1e1010';

        const isSuccess = await convocationManager.setAttendanceStatus(actor.id, sittingIdNotExist, 'notExist', 'Boris')
        expect(isSuccess).toBe(false);
    })
});


describe('update AR status', function () {
    it('it return true', async function () {
        const userRepository = getRepository(User);
        const actor = await userRepository.findOne({username: 'actor1@libriciel.fr'});
        const sittingsId = ['1fe63c21-f31b-4b1b-9fd9-6aaaa1d5ad7f', '9ccccf24-3d8e-4240-9bcf-bb962c1e1027'];
        let iSuccess = await convocationManager.setRead(actor.id, sittingsId);
        expect(iSuccess).toBe(true);


        let convocationsSaved = await getRepository(Convocation).find({
            where: {
                sittingId: In(sittingsId),
                userId: actor.id
            },
            relations: ['receivedTimestamp']
        });

        expect(convocationsSaved[0]).toMatchObject({
            receivedTimestampId: expect.any(String),
            isRead: true,
            receivedTimestamp : {
                id: any(String),
                createdAt: any(Date),
                filePathContent: any(String),
                filePathTsa: any(String)
            }
        })

        expect(fse.existsSync(convocationsSaved[0].receivedTimestamp.filePathContent)).toBeTruthy();
        expect(fse.existsSync(convocationsSaved[0].receivedTimestamp.filePathTsa)).toBeTruthy();

        const structureId = actor.structureId;
        fse.removeSync(config.tokenDirectory + structureId);
    })


    it('it return true no sittingsId', async function () {
        const userRepository = getRepository(User);
        const actor = await userRepository.findOne({username: 'actor1@libriciel.fr'});
        let iSuccess = await convocationManager.setRead(actor.id, null);
        expect(iSuccess).toBe(true);


        const structureId = actor.structureId;
        fse.removeSync(config.tokenDirectory + structureId);
    })


    it('it return empty no sittingsId', async function () {
        const userRepository = getRepository(User);
        const actor = await userRepository.findOne({username: 'actor1@libriciel.fr'});
        let iSuccess = await convocationManager.setRead(actor.id, []);
        expect(iSuccess).toBe(true);

        const structureId = actor.structureId;
        fse.removeSync(config.tokenDirectory + structureId);
    })

    it('it return true false sittingsId', async function () {
        const userRepository = getRepository(User);
        const actor = await userRepository.findOne({username: 'actor1@libriciel.fr'});
        const sittingsId = ['15463c21-f31b-4b1b-9fd9-6aaaa1d5ad7f'];
        let iSuccess = await convocationManager.setRead(actor.id, sittingsId);
        expect(iSuccess).toBe(true);

        const structureId = actor.structureId;
        fse.removeSync(config.tokenDirectory + structureId);
    });
});
