import {createConnection, getConnection} from "typeorm";
import loadFixturesHelper from "typeorm-fixtures-test";
import {projectManager} from "../../../src/service/0.2.0/ProjectManager";

beforeAll(async () => {
    await createConnection()
});

afterAll(async () => {
    getConnection().close().then(() => {
    });
})

beforeEach(async () => {
    try {
        await loadFixturesHelper();
    } catch (err) {
        console.log(err);
    }
});


describe('return active sittings from userId (Actor)', function () {
    it('it should return projects list', async function () {
        const archivedBureauId = '2fa63c21-f31b-4b1b-9fd9-6aaaa1d5ad7f';
        const projects = await projectManager.getArchivedProjectsFromSitting(archivedBureauId);
        expect(projects).toHaveLength(2);
        expect(projects[1]).toMatchObject({
            projet_document_id: expect.any(String),
            projet_id: expect.any(String),
            projet_name: "project_archived2",
            projet_rank: 1,
            ptheme_name: "Ecole"
        });

        expect(projects[1].annexes).toHaveLength(2);

        expect(projects[1].annexes[0]).toMatchObject({
            annexe_id: expect.any(String),
            annexe_name: 'fileAnnexArchived1.pdf'
        });
    });


    it('it should return an empty array cause not archived sitting', async function () {
        const notArchivedBureauId = '9ccccf24-3d8e-4240-9bcf-bb962c1e1027';
        const projects = await projectManager.getArchivedProjectsFromSitting(notArchivedBureauId);
        expect(projects).toHaveLength(0);
    });

    it('it should return an empty array cause sitting does not exist', async function () {
        const NotExists = '350c57cc-77a2-4d60-916d-7009e5a69766';
        const projects = await projectManager.getArchivedProjectsFromSitting(NotExists);
        expect(projects).toHaveLength(0);
    });

});
