import {createConnection, getConnection, getCustomRepository} from "typeorm";
import loadFixturesHelper from "typeorm-fixtures-test";
import {UserRepository} from "../../../src/repository/UserRepository";
import {sittingManager} from "../../../src/service/0.2.0/SittingManager";
import config from "../../../config";
import {stubTrue} from "lodash";

beforeAll(async () => {
    await createConnection()
});

afterAll(async () => {
    getConnection().close().then(() => {
    });
})

beforeEach(async () => {
    try {
        await loadFixturesHelper();
    } catch (err) {
        console.log(err);
    }
});


describe('return active sittings from userId (Actor)', function () {
    it('it should return added, modified, removed sittings', async function () {
        const userRepository = getCustomRepository(UserRepository);
        const actor1 = await userRepository.findOne({username: 'actor1@libriciel.fr'})
        const updatedSittingId = '1fe63c21-f31b-4b1b-9fd9-6aaaa1d5ad7f';
        const removedSittingId = '1ccccf24-3d8e-4240-9bcf-bb962c1e1026';

        const updateSittingsFeedback = await sittingManager.getFormattedSittings(
            actor1.id,
            [{seanceId: updatedSittingId, seanceRev: 1}, {seanceId: removedSittingId, seanceRev: 2}],
            config.legacy.groupActor
        );

        expect(updateSittingsFeedback).toMatchObject({
            toAdd: expect.any(Array),
            toModify: expect.any(Array),
            toRemove: expect.any(Array)
        });

        expect(updateSittingsFeedback.toAdd).toHaveLength(1);

        expect(updateSittingsFeedback.toAdd[0]).toMatchObject({
            seance_id: expect.any(String),
            seance_name: 'Bureau municipale',
            seance_date: expect.any(String),
            seance_rev: "1",
            seance_place: 'mairie',
            presentStatus: 'present',
            seance_document_id: expect.any(String),
            projets: expect.any(Array),
            convocation: expect.anything(),
            // isRemote: true,
        });

        const projects = updateSittingsFeedback.toAdd[0].projets
        expect(projects).toHaveLength(3)
        expect(projects[0]).toMatchObject({
            projet_id: expect.any(String),
            projet_document_id: expect.any(String),
            projet_user_id: null,
            projet_user_name: null,
            projet_name: 'Projet 1',
            projet_rank: 0,
            user: null,
            ptheme: {
                ptheme_name: 'Finance, Budget'
            },
            ptheme_name: 'Finance, Budget',
            annexes: expect.any(Array)
        });

        expect(projects[2]).toMatchObject({
            projet_id: expect.any(String),
            projet_document_id: expect.any(String),
            projet_user_id: expect.any(String),
            projet_user_name: 'Actor2 Libriciel',
            projet_name: 'Projet 3 avec annexes',
            projet_rank: 2,
            user: {
                projet_user_firstname: 'Actor2',
                projet_user_lastname: 'Libriciel'
            },
            ptheme: null,
            ptheme_name: 'Sans thème',
            annexes: expect.arrayContaining([
                {
                    "annexe_id": expect.any(String),
                    "annexe_name": "fileAnnex1.pdf",
                    "annexe_rank": 1,
                },
                {
                    "annexe_id": expect.any(String),
                    "annexe_name": "fileAnnex2.pdf",
                    "annexe_rank": 2,
                }
            ])
        });

        expect(updateSittingsFeedback.toModify).toHaveLength(1);
        const modifiedSitting = updateSittingsFeedback.toModify[0];
        expect(modifiedSitting).toMatchObject({
            seance_id: expect.any(String),
            seance_name: 'Bureau municipale modified',
            seance_date: "946816200000",
            seance_rev: "3",
            seance_place: 'mairie',
            projets: expect.any(Array),
            convocation: expect.anything(),
            presentStatus: 'present',
            seance_document_id: expect.any(String),
            // isRemote: true,
        })


        expect(updateSittingsFeedback.toRemove).toHaveLength(1);
        const removedSitting = updateSittingsFeedback.toRemove[0];
        expect(removedSitting).toMatchObject({
            seanceId: removedSittingId, seanceRev: expect.any(Number)
        })


    });


    it('it should return added sittings, we send nothing', async function () {
        const userRepository = getCustomRepository(UserRepository);
        const actor1 = await userRepository.findOne({username: 'actor1@libriciel.fr'})

        const updateSittingsFeedback = await sittingManager.getFormattedSittings(actor1.id, null, config.legacy.groupActor);

        expect(updateSittingsFeedback).toMatchObject({
            toAdd: expect.any(Array),
            toModify: expect.any(Array),
            toRemove: expect.any(Array)
        })

        expect(updateSittingsFeedback.toAdd).toHaveLength(2);
        expect(updateSittingsFeedback.toModify).toHaveLength(0);
        expect(updateSittingsFeedback.toRemove).toHaveLength(0);

    });


});



describe('return active sittings from userId (Employee)', function () {
    it('it should return added sittings', async function () {
        const userRepository = getCustomRepository(UserRepository);
        const employeeLs = await userRepository.findOne({username: 'employee@libriciel.fr'})
        const updateSittingsFeedback = await sittingManager.getFormattedSittings(employeeLs.id, null, config.legacy.groupEmployee);

        expect(updateSittingsFeedback).toMatchObject({
            toAdd: expect.any(Array),
            toModify: expect.any(Array),
            toRemove: expect.any(Array)
        })

        expect(updateSittingsFeedback.toAdd).toHaveLength(1);
        expect(updateSittingsFeedback.toModify).toHaveLength(0);
        expect(updateSittingsFeedback.toRemove).toHaveLength(0);

        const addedSitting = updateSittingsFeedback.toAdd[0];
        expect(addedSitting.projets).toHaveLength(3);
        expect(addedSitting).toMatchObject({
            seance_id: expect.any(String),
            seance_name: 'Bureau municipale',
            seance_date: expect.any(String),
            seance_rev: '1',
            seance_place: 'mairie',
            presentStatus: null,
            seance_document_id: expect.any(String),
            projets: expect.any(Array),
            invitation: expect.anything(),
            // isRemote: true,
        })



    });
});




describe('return active sittings from userId (Guest)', function () {
    it('it should return added sittings', async function () {
        const userRepository = getCustomRepository(UserRepository);
        const guestLs = await userRepository.findOne({username: 'guest@libriciel.fr'})
        const updateSittingsFeedback = await sittingManager.getFormattedSittings(guestLs.id, null, config.legacy.groupGuest);

        expect(updateSittingsFeedback).toMatchObject({
            toAdd: expect.any(Array),
            toModify: expect.any(Array),
            toRemove: expect.any(Array)
        })

        expect(updateSittingsFeedback.toAdd).toHaveLength(1);
        expect(updateSittingsFeedback.toModify).toHaveLength(0);
        expect(updateSittingsFeedback.toRemove).toHaveLength(0);

        const addedSitting = updateSittingsFeedback.toAdd[0];
        expect(addedSitting.projets).not.toBeDefined();
        expect(addedSitting).toMatchObject({
            seance_id: expect.any(String),
            seance_name: 'Bureau municipale',
            seance_date: expect.any(String),
            seance_rev: '1',
            seance_place: 'mairie',
            presentStatus: null,
            seance_document_id: expect.any(String),
            invitation: expect.anything(),
            // isRemote: true,
        })



    });
});
