import {createConnection, getConnection, getCustomRepository} from "typeorm";
import loadFixturesHelper from "typeorm-fixtures-test";
import {UserRepository} from "../../../src/repository/UserRepository";
import {sittingManager} from "../../../src/service/0.2.0/SittingManager";
import config from "../../../config";
import {stubTrue} from "lodash";
import {SittingRepository} from "../../../src/repository/SittingRepository";

beforeAll(async () => {
    await createConnection()
});

afterAll(async () => {
    getConnection().close().then(() => {
    });
})

beforeEach(async () => {
    try {
        await loadFixturesHelper();
    } catch (err) {
        console.log(err);
    }
});


describe('return sittings with convocations', function () {
    it('it should return sittings with convocations', async function () {
        const sittingRepository = getCustomRepository(SittingRepository);
        const sittings  = await sittingRepository.getSittingsWithConvocations(['9ccccf24-3d8e-4240-9bcf-bb962c1e1027']);


        expect(sittings).toHaveLength(1);
        expect(sittings[0].convocations).not.toBeFalsy();
        expect(sittings[0].convocations[0]).not.toBeFalsy();

    });



});
