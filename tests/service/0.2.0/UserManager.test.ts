import {createConnection, getConnection} from "typeorm";
import loadFixturesHelper from "typeorm-fixtures-test";
import any = jasmine.any;
import {userManager} from "../../../src/service/0.2.0/UserManager";
import {attendanceManager} from "../../../src/service/0.2.0/AttendanceManager";

beforeAll(async () => {
    await createConnection()
});

afterAll(async () => {
    getConnection().close().then(() => {
    });
})

beforeEach(async () => {
    try {
        await loadFixturesHelper();
    } catch (err) {
        console.log(err);
    }
});


describe('return actors from sitting', function () {
    it('it should return actors list with details', async function () {
        const sittingBureauId = '9ccccf24-3d8e-4240-9bcf-bb962c1e1027';
        const actors = await userManager.getActorFromSitting(sittingBureauId);
        expect(actors).toHaveLength(7);
        expect(actors[0]).toMatchObject({
            id: any(String),
            firstname: 'Actor1',
            lastname: 'Libriciel',
            username: 'actor1@libriciel.fr',
            groupepolitique_id: any(Number),
            groupepolitique_name: 'majorite',
            seance_id: sittingBureauId
        })
    });
});


describe('return available mandator for a sitting', function () {
    it('it should return actors', async function () {
        const sittingBureauId = '9ccccf24-3d8e-4240-9bcf-bb962c1e1027';
        const availableMandators = await attendanceManager.getAvailableMandators(sittingBureauId, null);

        expect(availableMandators).toHaveLength(6);

    })

});
