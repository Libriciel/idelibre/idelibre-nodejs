import {createConnection, getConnection, getCustomRepository} from "typeorm";
import loadFixturesHelper from "typeorm-fixtures-test";
import {SittingRepository} from "../../../src/repository/SittingRepository";
import {UserRepository} from "../../../src/repository/UserRepository";

beforeAll(async () => {
    await createConnection()
});

afterAll(async () => {
    getConnection().close().then(() => {
    });
})

beforeEach(async () => {
    try {
        await loadFixturesHelper();
    } catch (err) {
        console.log(err);
    }
});


describe('return actors from a given sitting', function () {
    it('it should return all actors from this sitting', async function () {
        const userRepository = getCustomRepository(UserRepository);
        const actorsInSitting = await userRepository.getActorsBySittingId('9ccccf24-3d8e-4240-9bcf-bb962c1e1027');
        expect(actorsInSitting).toHaveLength(7);

    });


});
