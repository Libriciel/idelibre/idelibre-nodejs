import {createConnection, getConnection, getCustomRepository, getRepository} from "typeorm";
import loadFixturesHelper from "typeorm-fixtures-test";
import {authenticationService} from "../../src/service/AuthenticationService";
import {UserRepository} from "../../src/repository/UserRepository";
import config from "../../config";
import {User} from "../../src/entity/User";
import {jwtService} from "../../src/service/JwtService";
import {Configuration} from "../../src/entity/Configuration";
import Config from "../../config";
import { v4 as uuidV4 } from 'uuid';


beforeAll(async () => {
    await createConnection()
});

afterAll(async () => {
    getConnection().close().then(() => {
    });
})

beforeEach(async () => {
    try {
        await loadFixturesHelper();
    } catch (err) {
        console.log(err);
    }
});

describe('Authentication: verifyPassword', function () {
    it('should return the user with regular login method', async function (done) {
        const user = await authenticationService.verifyPassword('actor1@libriciel.fr', 'password');


        expect(user).toHaveProperty('userId', expect.anything());
        expect(user).toHaveProperty('group_id', config.legacy.groupActor);
        done();
    });

    it('should be null cause the user does not exists', async function (done) {
        const user = await authenticationService.verifyPassword('falseUsername@libriciel.fr', 'password');
        expect(user).toBe(null);
        done();
    });

    it('should be null cause the password is false', async function (done) {
        const user = await authenticationService.verifyPassword('actor1@libriciel.fr', 'passwordError');
        expect(user).toBe(null);
        done();
    });

    it('should be null cause null user', async function (done) {
        const user = await authenticationService.verifyPassword(null, 'passwordError');
        expect(user).toBe(null);
        done();
    });

    it('should be null cause empty user', async function (done) {
        const user = await authenticationService.verifyPassword('', 'passwordError');
        expect(user).toBe(null);
        done();
    });

    it('should be null cause null password', async function (done) {
        const user = await authenticationService.verifyPassword('actor1@libriciel.fr', null);
        expect(user).toBe(null);
        done();
    });

    it('should be null cause empty password', async function (done) {
        const user = await authenticationService.verifyPassword('actor1@libriciel.fr', '');
        expect(user).toBe(null);
        done();
    });

    it('should return user and update password cause legacy password', async function (done) {
        const user = await authenticationService.verifyPassword('actorSha1@libriciel.fr', 'passwordLegacy');
        expect(user).toHaveProperty('userId', expect.anything());
        expect(user).toHaveProperty('group_id', expect.anything());

        const userRepository = getCustomRepository(UserRepository)
        let updatedUser = await userRepository.findOne({username: 'actorSha1@libriciel.fr'});
        expect(updatedUser.password[0]).toBe('$')

        const userWithArgonPassword = await authenticationService.verifyPassword('actorSha1@libriciel.fr', 'passwordLegacy');
        expect(userWithArgonPassword).toHaveProperty('userId', expect.anything());
        expect(userWithArgonPassword).toHaveProperty('group_id', expect.anything());

        done();
    });

    it('should be false cause inactive user', async function (done) {
        const user = await authenticationService.verifyPassword('actorInactive@libriciel.fr', 'password');
        expect(user).toBe(null);
        done();
    });


    describe('Authentication with token', function () {
        it('should return data to send to client', async function (done) {
            const user = await getRepository(User).findOne({username: 'actor1@libriciel.fr'})
            const token = await jwtService.sign({
                userId: user.id,
                group_id: Config.legacy.groupActor,
                structureId: user.structureId
            })

            const toSend = await authenticationService.authenticateWithToken(token)

            expect(toSend).not.toBe(null)

            expect(toSend).toMatchObject({
                success: true,
                message: 'token',
                token: token,
                userId: user.id,
                group_id: config.legacy.groupActor,
                groupId: config.legacy.groupActor,
                configuration: expect.any(Configuration)
            });


            done();
        });

        it('should return null cause user does not exists', async function (done) {

            const token = await jwtService.sign({
                userId: uuidV4,
                group_id: Config.legacy.groupActor,
                structureId: uuidV4
            })

            const toSend = await authenticationService.authenticateWithToken(token)
            expect(toSend).toBe(null);

            done();
        });

        it('should return null cause user is not active', async function (done) {

            const user = await getRepository(User).findOne({username: 'actorInactive@libriciel.fr'})
            const token = await jwtService.sign({
                userId: user.id,
                group_id: Config.legacy.groupActor,
                structureId: user.structureId
            })

            const toSend = await authenticationService.authenticateWithToken(token)

            expect(user).not.toBe(null);
            expect(toSend).toBe(null);

            done();
        });

        it('should return null cause user jwt invalidate', async function (done) {

            const user = await getRepository(User).findOne({username: 'actor_libriciel_with_jwt_invalid_before_expired@libriciel.fr'})
            const token = await jwtService.sign({
                userId: user.id,
                group_id: Config.legacy.groupActor,
                structureId: user.structureId
            })

            const toSend = await authenticationService.authenticateWithToken(token)

            expect(user).not.toBe(null);
            expect(toSend).toBe(null);

            done();
        });


        it('should return null cause user jwt invalidate before not expired', async function (done) {

            const user = await getRepository(User).findOne({username: 'actor_libriciel_with_jwt_invalid_before@libriciel.fr'})
            const token = await jwtService.sign({
                userId: user.id,
                group_id: Config.legacy.groupActor,
                structureId: user.structureId
            })

            const toSend = await authenticationService.authenticateWithToken(token)

            expect(user).not.toBe(null);
            expect(toSend).not.toBe(null);

            done();
        });





    })


});


