import {createConnection, getConnection, getRepository} from "typeorm";
import loadFixturesHelper from "typeorm-fixtures-test";
import {InvalidatedTokenError, jwtService} from "../../src/service/JwtService";
import {JsonWebTokenError} from "jsonwebtoken";
import {User} from "../../src/entity/User";
import Config from "../../config";

beforeAll(async () => {
    await createConnection()
});

afterAll(async () => {
    getConnection().close().then(() => {
    });
})

beforeEach(async () => {
    try {
        await loadFixturesHelper();
    } catch (err) {
        console.log(err);
    }
});


describe('Jwt create sign token and decode it', function () {
    it('it should create token and verification should work', async function (done) {
        const user = await getRepository(User).findOne({username: 'actor1@libriciel.fr'})
        const data = {
            userId: user.id,
            group_id: Config.legacy.groupActor,
            structureId: user.structureId
        };
        const token = await jwtService.sign(data)

        expect(token).not.toBeNull()
        const decoded = await jwtService.verify(token);
        expect(decoded).toMatchObject(data);
        done();
    });


    it('it should throw an error cause token signature is false', async function (done) {
        const token = await jwtService.signPromise({foo: 'bar'}, 'badSecret');
        expect(token).not.toBeNull()
        await expect(jwtService.verify(token)).rejects.toThrow(JsonWebTokenError);
        done();
    });

    it('it should throw an error cause token is modified', async function (done) {
        const token = await jwtService.sign({foo: 'bar'});
        expect(token).not.toBeNull()
        const modifiedToken = token.substr(4, 1);
        await expect(jwtService.verify(modifiedToken)).rejects.toThrow(JsonWebTokenError);
        done();
    });

    it('it should throw an error cause null token', async function (done) {
        await expect(jwtService.verify(null)).rejects.toThrow(JsonWebTokenError);
        done();
    });


    it('it should throw an error cause undefined token', async function (done) {
        await expect(jwtService.verify(undefined)).rejects.toThrow(JsonWebTokenError);
        done();
    });


    it('it should create token and verification should work cause iat < jwtInvalidBefore', async function (done) {
        const userJwtValid = await getRepository(User).findOne({username: 'actor_libriciel_with_jwt_invalid_before@libriciel.fr'})
        const token = await jwtService.sign({userId: userJwtValid.id})
        const decoded = await jwtService.verify(token);

        expect(decoded).toMatchObject({userId: userJwtValid.id});
        done();
    });


    it('it should create token and verification should NOT work cause iat > jwtInvalidBefore', async function (done) {
        const userJwtInvalid = await getRepository(User).findOne({username: 'actor_libriciel_with_jwt_invalid_before_expired@libriciel.fr'})
        const token = await jwtService.sign({userId: userJwtInvalid.id})
        await expect(jwtService.verify(token)).rejects.toThrow(InvalidatedTokenError);
        done();
    });


});


