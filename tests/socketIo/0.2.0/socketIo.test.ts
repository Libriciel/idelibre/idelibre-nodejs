import {initSocketIo} from "../../../src/socketIo/0.2.0/initSocketIo";
import {io, Socket} from "socket.io-client";
import {Server} from "socket.io";
import * as http from "http";
import {createConnection, getConnection, getRepository} from "typeorm";
import loadFixturesHelper from "typeorm-fixtures-test";
import {authenticationService} from "../../../src/service/AuthenticationService";
import {jwtService} from "../../../src/service/JwtService";
import {socketManager} from "../../../src/service/SocketManager";
import {User} from "../../../src/entity/User";
import {AnnotationFromClient} from "../../../src/service/0.2.0/Annotation/AnnotationFromClient";
import any = jasmine.any;
import {Annotation} from "../../../src/entity/Annotation";
import Config from "../../../config";
import { v4 as uuidV4 } from 'uuid';


let socket: Socket;
let httpServer;
let httpServerHost = '127.0.0.1';
let httpServerPort = 1234;
let socketNamespace = '0.2.0'
let ioServer;


beforeAll(async (done) => {
    await createConnection();
    httpServer = http.createServer().listen(httpServerPort, httpServerHost)
    ioServer = new Server(httpServer, {cors: {origin: "*"}});
    initSocketIo(ioServer);
    done();
});


afterAll(async (done) => {
    ioServer.close();
    httpServer.close();
    await getConnection().close()
    done();
});


beforeEach(async (done) => {

    try {
        await loadFixturesHelper();
    } catch (err) {
        console.log(err);
    }

    socket = io(`http://${httpServerHost}:${httpServerPort}/${socketNamespace}`)
    /* socket.on('connect', () => {
         done();
     });*/
    done();
});

/**
 * Run after each test
 */
afterEach((done) => {
    socket.disconnect();
    socketManager.clear();
    done();
});


async function getToken(username: string, password: string): Promise<string> {
    let loggedInUser = await authenticationService.verifyPassword(username, password);
    return jwtService.sign(loggedInUser)
}



describe('socket.io communication', () => {
    it('should received the hello message', (done) => {
        socket.on('hello', (data) => {
            expect(data).toMatchObject({message: 'Welcome'})
            done();
        })
    });


    it('should do something', (done) => {
        socket.emit('updateSeances', {foo: 'bar'});
        socket.on('tokenError', (data) => {
            done();
        })
    });
});


describe('socket.io authentication', () => {
    it('should return error object cause wrong login', (done) => {

        socket.on('authFeedback', (data) => {
            expect(data).toStrictEqual({success: false, message: '', token: null, userId: null, isLoginWithTokenAllowed: true});
            done();
        })

        socket.emit('authenticate', {username: 'notExist', password: 'passwordFalse', suffix: 'suffixFalse'});
    });


    it('should return success object and be added to socketManager list', (done) => {

        socket.on('authFeedback', (data) => {
            expect(data).toMatchObject({
                success: true,
                message: 'token',
                token: expect.any(String),
                userId: expect.any(String),
                group_id: expect.any(String),
                configuration: {
                    isSharedAnnotation: true
                }
            });

            expect(socketManager.getSocketByUserId(data.userId)).toBeTruthy();
            done();
        })

        socket.emit('authenticate', {username: 'actor1', password: 'password', suffix: 'libriciel.fr'});
    });

});


describe('socket.io authentication with token', () => {
    it('should return error object cause fakeToken', async (done) => {

        const token = await jwtService.sign({
            userId: uuidV4,
            group_id: Config.legacy.groupActor,
            structureId: uuidV4
        })
        socket.on('authFeedback', (data) => {
            expect(data).toStrictEqual({success: false, message: 'Token incorrect', token: null, userId: null, isLoginWithTokenAllowed: true});
            done();
        })

        socket.emit('authenticate', {username: '', password: '', suffix: '', token: token});
    });


    it('should return success object and be added to socketManager list', async(done) => {

        const user = await getRepository(User).findOne({username: 'actor1@libriciel.fr'})
        const token = await jwtService.sign({
            userId: user.id,
            group_id: Config.legacy.groupActor,
            structureId: user.structureId
        })

        socket.on('authFeedback', (data) => {
            expect(data).toMatchObject({
                success: true,
                message: 'token',
                token: token,
                userId: user.id,
                group_id: expect.any(String),
                configuration: {
                    isSharedAnnotation: true
                }
            });

            expect(socketManager.getSocketByUserId(data.userId)).toBeTruthy();
            done();
        })

        socket.emit('authenticate', {username: 'tutu', password: null, suffix: null, token: token});
    });


});



describe('socket.io checkToken', () => {
    it('should return error object cause no token', async (done) => {
        socket.emit('updateSeances', {});
        socket.on('tokenError', (data) => {
            expect(JSON.parse(data)).toMatchObject({"success": false, "message": "token error"})
            done();
        })
    });

    it('should return error object cause falseToken', async (done) => {
        socket.on('tokenError', (data) => {
            expect(JSON.parse(data)).toMatchObject({"success": false, "message": "token error"})
            done();
        })
        socket.emit('updateSeances', {token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6InVzZXIiLCJpYXQiOjE1MTYyMzkwMjJ9.wsS59oQlBm2hjsRZ7J9c1rRvrff16LtpzpqSaZvqR9Y'});
    });


});


describe('socket.io updateSeances', () => {

    it('should return two sitting to add (Actor)', async (done) => {

        const token = await getToken('actor1@libriciel.fr', 'password')

        socket.on('tokenError', () => {
            fail('it should not reach here');
        })

        socket.on('updateSeancesInvitesFeedback', (data) => {
            fail('it should not reach here (ACTOR');
        });


        socket.on('updateSeancesAdministratifsFeedback', (data) => {
            fail('it should not reach here (ACTOR');
        });


        socket.on('updateSeancesFeedback', (data) => {
            let updateSittings = JSON.parse(data);
            expect(updateSittings).toMatchObject({
                toAdd: expect.any(Array),
                toModify: expect.any(Array),
                toRemove: expect.any(Array)
            });

            expect(updateSittings.toAdd).toHaveLength(2);

            const firstAddedSitting = updateSittings.toAdd[0];
            expect(firstAddedSitting.otherdocs).toHaveLength(2);
            firstAddedSitting.otherdocs[0].name = 'mon premier doc'

            done();
        })

        socket.emit('updateSeances', {token: token});

    });


    it('should return one sitting to add (Employee)', async (done) => {

        const token = await getToken('employee@libriciel.fr', 'password')

        socket.on('tokenError', () => {
            fail('it should not reach here');
        })

        socket.on('updateSeancesFeedback', (data) => {
            fail('it should not reach here (EMPLOYEE');
        });

        socket.on('updateSeancesInvitesFeedback', (data) => {
            fail('it should not reach here (EMPLOYEE');
        });

        socket.on('updateSeancesAdministratifsFeedback', (data) => {
            let updateSittings = JSON.parse(data);

            expect(updateSittings).toMatchObject({
                toAdd: expect.any(Array),
                toModify: expect.any(Array),
                toRemove: expect.any(Array)
            });

            expect(updateSittings.toAdd).toHaveLength(1);
            expect(updateSittings.toAdd[0].otherdocs).toHaveLength(2);

            done();
        })

        socket.emit('updateSeances', {token: token});

    });


    it('should return one sitting to add (Guest)', async (done) => {

        const token = await getToken('guest@libriciel.fr', 'password')

        socket.on('tokenError', () => {
            fail('it should not reach here');
        })

        socket.on('updateSeancesFeedback', (data) => {
            fail('it should not reach here (Guest');
        });

        socket.on('updateSeancesAdministratifsFeedback', (data) => {
            fail('it should not reach here (Guest');
        });

        socket.on('updateSeancesInvitesFeedback', (data) => {

            let updateSittings = JSON.parse(data);
            expect(updateSittings).toMatchObject({
                toAdd: expect.any(Array),
                toModify: expect.any(Array),
                toRemove: expect.any(Array)
            });

            expect(updateSittings.toAdd).toHaveLength(1);

            done();
        })

        socket.emit('updateSeances', {token: token});

    });

});


describe('presentStatus', () => {
    it('should emit confirmPresenceFeedback', async (done) => {
        const token = await getToken('actor1@libriciel.fr', 'password');
        const sittingBureauId = '9ccccf24-3d8e-4240-9bcf-bb962c1e1027';
        socket.on('confirmPresenceFeedback', (data) => {
            expect(data).toMatchObject({success: true})
            done();
        });

        socket.emit('sendPresence', {token: token, seanceId: sittingBureauId, presentStatus: 'present', procuration_name: 'Boris'})

    })
});


describe('ARs', () => {
    it('should emit convocationReadFeedBack', async (done) => {

        const token = await getToken('actor1@libriciel.fr', 'password');
        const sittingBureauId = '9ccccf24-3d8e-4240-9bcf-bb962c1e1027';
        socket.on('convocationReadFeedBack', (data) => {
            expect(true).toBeTruthy();
            done();
        });

        socket.emit('ARs', {token: token, seancesId: [sittingBureauId]});

    })
});


describe('archivedSeancesList', () => {
    it('should emit archivedSeancesListFeedBack with sittings', async (done) => {

        const token = await getToken('actor1@libriciel.fr', 'password');
        socket.on('archivedSeancesListFeedBack', (data) => {
            const sittings = JSON.parse(data);
            expect(sittings).toHaveLength(1);
            expect(sittings[0]).toMatchObject({
                seance_id: expect.any(String),
                seance_name: 'Bureau municipale archivée',
                seance_date: expect.any(String),
                seance_document_id: expect.any(String)
            });
            done();
        });

        socket.emit('archivedSeancesList', {token: token});

    })
});



describe('archivedProjetsList', () => {
    it('should emit archivedProjetsListFeedBack with  2 projects', async (done) => {

        const token = await getToken('actor1@libriciel.fr', 'password');
        socket.on('archivedProjetsListFeedBack', (data) => {
            const projects = JSON.parse(data);
            expect(projects).toHaveLength(2);
            done();
        });

        const archivedBureauId = '2fa63c21-f31b-4b1b-9fd9-6aaaa1d5ad7f';
        socket.emit('archivedProjetsList', {token: token, seanceId: archivedBureauId});

    })
});


describe('getUserList', () => {
    it('should emit userList with  4 actors', async (done) => {
        const token = await getToken('actor1@libriciel.fr', 'password');

        socket.on('userList', (data) => {
            const actors = JSON.parse(data);
            expect(actors).toHaveLength(7);
            done();
        });

        const sittingBureauId = '9ccccf24-3d8e-4240-9bcf-bb962c1e1027';
        socket.emit('getUserList', {token: token, seanceId: sittingBureauId});

    })
});



describe('getAnnotations', () => {
    it('should emit getAnnotationsFeedback with 3 annotations', async (done) => {
        const token = await getToken('actor1@libriciel.fr', 'password');

        socket.on('getAnnotationsFeedback', (data) => {
            const annotations = JSON.parse(data);
            expect(annotations).toHaveLength(3);
            done();
        });

        socket.emit('getAnnotations', {token: token});

    })
});



describe('sendAnnotationRead', () => {
    it('should emit sendAnnotationReadFeedback', async (done) => {
        const token = await getToken('actor1@libriciel.fr', 'password');
        socket.on('sendAnnotationReadFeedback', (data) => {
            expect(data).toMatchObject({});
            done();
        });

        socket.emit('sendAnnotationRead', {token: token, annotationIds: []});



    });
});


describe('readAnnotationsConnect', () => {
    it('should emit readAnnotationsConnectFeedback', async (done) => {
        const token = await getToken('actor1@libriciel.fr', 'password');
        socket.on('readAnnotationsConnectFeedback', (data) => {
            expect(data).toMatchObject({});
            done();
        });

        socket.emit('readAnnotationsConnect', {token: token, annotationIds: []});
    });
});


describe('annotation', () => {
    it('Add a new shared Annotation should emit addNewAnnotationFeedback and newAnnotation to sharedUsers', async (done) => {
        const token = await getToken('actor1@libriciel.fr', 'password');
        const actor = await getRepository(User).findOne({username: 'actor1@libriciel.fr'});
        const actor2 = await getRepository(User).findOne({username: 'actor2@libriciel.fr'});
        const actor3 = await getRepository(User).findOne({username: 'actor3@libriciel.fr'});
        const annex1Id = "cb712bf9-5370-4c23-9048-8895a3286fa5"


        let socketActor2 = io(`http://${httpServerHost}:${httpServerPort}/${socketNamespace}`);

        let annotationClient = new AnnotationFromClient();
        annotationClient.annotation_id = 'eb712bf9-5370-4c23-9048-8895a3286fa3'
        annotationClient.annotation_author_id = actor.id
        annotationClient.annotation_page = 1
        annotationClient.originType = "Annexe"
        annotationClient.originId = annex1Id
        annotationClient.annotation_shareduseridlist = [actor2.id, actor3.id]
        annotationClient.annotation_text = "New annotation shared annotations"
        annotationClient.annotation_date = (new Date()).getTime();
        annotationClient.annotation_author_name = 'not real name'
        annotationClient.annotation_rect = {top: 1, left: 1, bottom: 1, right: 1}
        let annotationJson = JSON.stringify(annotationClient)

        socket.on('addNewAnnotationFeedback', (data) => {
            expect(JSON.stringify(data)).toBe('{}')
        });

        socketActor2.on('newAnnotation', (data) => {
            expect(JSON.parse(data)).toMatchObject({
                userId: actor2.id,
                annotation: {
                    annotation_id: 'eb712bf9-5370-4c23-9048-8895a3286fa3',
                    annotation_author_id: any(String),
                    annotation_page: 1,
                    originType: 'Annexe',
                    originId: 'cb712bf9-5370-4c23-9048-8895a3286fa5',
                    annotation_shareduseridlist: [any(String), any(String)],
                    annotation_text: 'New annotation shared annotations',
                    annotation_date: any(Number),
                    annotation_author_name: any(String),
                    annotation_rect: {top: 1, left: 1, bottom: 1, right: 1}
                },
                originType: 'Annexe',
                originId: 'cb712bf9-5370-4c23-9048-8895a3286fa5'
            })
            socketActor2.disconnect();
            done();
        })


        socketActor2.on("connect", () => {
            socketManager.associateClientSocket(actor2.id, socketActor2.id);
            socket.emit('annotation', {token: token, annotation: [annotationJson]});
        })
    });


    it('update a shared Annotation should emit addNewAnnotationFeedback to author, newAnnotation and sharedAnnotationDeleted to sharedUsers', async (done) => {
        const token = await getToken('actor1@libriciel.fr', 'password');
        const actor = await getRepository(User).findOne({username: 'actor1@libriciel.fr'});
        const actor2 = await getRepository(User).findOne({username: 'actor2@libriciel.fr'});
        const actor3 = await getRepository(User).findOne({username: 'actor3@libriciel.fr'});
        const actor4 = await getRepository(User).findOne({username: 'actor4@libriciel.fr'});
        const project2Id = "bd67266e-c858-41e3-98b6-087ebd5835c3"
        const existingAnnotationId = "4d4c6fcd-2f48-4e2d-b9ef-bdf408d03faa"


        let socketActor2 = io(`http://${httpServerHost}:${httpServerPort}/${socketNamespace}`);

        let annotationClient = new AnnotationFromClient();
        annotationClient.annotation_id = existingAnnotationId
        annotationClient.annotation_author_id = actor.id
        annotationClient.annotation_page = 1
        annotationClient.originType = "Projet"
        annotationClient.originId = project2Id
        annotationClient.annotation_shareduseridlist = [actor2.id, actor4.id]
        annotationClient.annotation_text = "modified Annotation"
        annotationClient.annotation_date = (new Date()).getTime();
        annotationClient.annotation_author_name = 'not real name'
        annotationClient.annotation_rect = {top: 1, left: 1, bottom: 1, right: 1}
        let annotationJson = JSON.stringify(annotationClient)

        socket.on('addNewAnnotationFeedback', (data) => {
            expect(JSON.stringify(data)).toBe('{}')
        });

        socketActor2.on('sharedAnnotationDeleted', (data) => {
            expect(data).toStrictEqual({ annotation_Id: existingAnnotationId });

        });

        socketActor2.on('newAnnotation', (data) => {
            expect(JSON.parse(data)).toMatchObject({
                userId: actor2.id,
                annotation: {
                    annotation_id: existingAnnotationId,
                    annotation_author_id: any(String),
                    annotation_page: 1,
                    originType: 'Projet',
                    originId: project2Id,
                    annotation_shareduseridlist: [any(String), any(String)],
                    annotation_text: 'modified Annotation',
                    annotation_date: any(Number),
                    annotation_author_name: any(String),
                    annotation_rect: {top: 1, left: 1, bottom: 1, right: 1}
                },
                originType: 'Projet',
                originId: project2Id
            })
            socketActor2.disconnect();
            done();
        })


        socketActor2.on("connect", () => {
            socketManager.associateClientSocket(actor2.id, socketActor2.id);
            socket.emit('annotation', {token: token, annotation: [annotationJson]});
        })
    });


});



describe('deleteAnnotations (private Annotation)', () => {
    it('should emit deleteAnnotationFeedback', async (done) => {
        const privateAnnotationId = '1a0db141-f414-4393-ab61-45512bd6c9bc'
        const token = await getToken('actor1@libriciel.fr', 'password');

        socket.on('deleteAnnotationFeedback', async(data) => {
            expect(JSON.stringify(data)).toBe('{}');
            const deletedAnnotation = await getRepository(Annotation).findOne(privateAnnotationId);
            expect(deletedAnnotation).toBeFalsy();
            done();
        });

        socket.emit('deleteAnnotations', {token: token ,ids: [privateAnnotationId]});

    })
});



describe('deleteAnnotations (shared Annotation)', () => {
    it('should emit deleteAnnotationFeedback and sharedAnnotationDeleted', async (done) => {
        const sharedAnnotationId = '4d4c6fcd-2f48-4e2d-b9ef-bdf408d03faa'
        const token = await getToken('actor1@libriciel.fr', 'password');
        const actor2 = await getRepository(User).findOne({username: 'actor2@libriciel.fr'});

        let socketActor2 = io(`http://${httpServerHost}:${httpServerPort}/${socketNamespace}`);



        socket.on('deleteAnnotationFeedback', (data) => {
            expect(JSON.stringify(data)).toBe('{}');
        });


        socketActor2.on("sharedAnnotationDeleted", async(data) => {
            expect(data).toMatchObject({
                annotation_Id: sharedAnnotationId
            })
            const deletedAnnotation = await getRepository(Annotation).findOne(sharedAnnotationId);
            expect(deletedAnnotation).toBeFalsy();
            socketActor2.disconnect();
            done();
        });

        socketActor2.on("connect", () => {
            socketManager.associateClientSocket(actor2.id, socketActor2.id);
            socket.emit('deleteAnnotations', {token: token ,ids: [sharedAnnotationId]});
        })


    })
});


